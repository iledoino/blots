#include "lu.h"
#include <stdexcept>

namespace blots
{

/*
 * Constructor
 */
LU::LU(const std::size_t& dim_) : dim(dim_), pcopy(dim_) {}

int
LU::decomp(double* M, std::size_t* p, const double& zero, double& upperCond)
{
    std::size_t t, aux;
    double tmp;
    int returned_value = 1;

    for (std::size_t i = 0; i != dim; ++i)
        p[i] = i;

    upperCond = 1.0;
    for (std::size_t i = 0, iM = 0; i < dim - 1; i++, iM += dim) {
        t = i;
        for (std::size_t j = i + 1, jM = (i + 1) * dim; j < dim; ++j, jM += dim)
            if (fabs(M[t * dim + i]) < fabs(M[jM + i]))
                t = j;

        if (t != i) {
            aux = p[i];
            p[i] = p[t];
            p[t] = aux;

            t = t * dim;
            for (std::size_t j = 0; j != dim; ++j) {
                tmp = M[iM + j];
                M[iM + j] = M[t + j];
                M[t + j] = tmp;
            }
        }

        if ((M[iM + i] < zero) && (-M[iM + i] < zero)) {
            returned_value = 0;
        }

        double inv_diag_value = 1. / M[iM + i];
        upperCond *= inv_diag_value;
        for (std::size_t j = i + 1, jM = (i + 1) * dim; j < dim;
                j++, jM += dim) {
            M[jM + i] *= inv_diag_value;
            for (t = i + 1; t < dim; t++)
                M[jM + t] -= M[jM + i] * M[iM + t];
        }
    }
    upperCond *= 2.;

    if (returned_value == 0)
        std::cerr << "LU::decomp: Error: found a diagonal value with "
                     "norm smaller than "
                  << zero << "." << std::endl;

    return returned_value;
}

int
LU::decomp(std::vector<double>& M, std::vector<std::size_t>& p,
        double& upperCond, double zero)
{
    return decomp(M.data(), p.data(), zero, upperCond);
}

void
LU::forwBackSubs(
        double* M, double* RHS, std::size_t RHSnOfColumns, std::size_t* p)
{
    double aux;

    permProd(RHS, RHSnOfColumns, p);

    for (std::size_t i = 1, iM = dim, iRHS = RHSnOfColumns; iM != dim * dim;
            iM += dim, iRHS += RHSnOfColumns, ++i)
        for (std::size_t jRHS = 0, jM = 0; jM != i;
                jRHS += RHSnOfColumns, ++jM) {
            aux = M[iM + jM];
            for (std::size_t t = 0; t < RHSnOfColumns; t++)
                RHS[iRHS + t] -= aux * RHS[jRHS + t];
        }

    for (std::size_t i = dim, iM = (i - 1) * i, iRHS = (i - 1) * RHSnOfColumns;
            i > 0; iM -= dim, iRHS -= RHSnOfColumns, --i) {
        for (std::size_t j = dim, jM = (j - 1), jRHS = jM * RHSnOfColumns;
                j > i; j = jM, --jM, jRHS -= RHSnOfColumns) {
            aux = M[iM + jM];
            for (std::size_t t = 0; t < RHSnOfColumns; ++t)
                RHS[iRHS + t] -= aux * RHS[jRHS + t];
        }

        double inv_diag_value = 1. / M[iM + i - 1];
        for (std::size_t t = 0; t < RHSnOfColumns; ++t)
            RHS[iRHS + t] *= inv_diag_value;
    }
}

void
LU::forwBackSubs(std::vector<double>& M, std::vector<double>& RHS,
        std::vector<std::size_t>& p)
{
    std::size_t RHSnOfColumns = static_cast<std::size_t>(RHS.size() / dim);
    forwBackSubs(M.data(), RHS.data(), RHSnOfColumns, p.data());
}

void
LU::forwBackSubsT(double* M, double* RHS, std::size_t RHSnOfRows,
        std::size_t* p, std::size_t RHSLD)
{
    std::size_t RHSDim(dim + RHSLD);
    for (std::size_t tRHS = 0, tRHSEnd = RHSnOfRows * RHSDim; tRHS < tRHSEnd;
            tRHS += RHSDim) {
        for (std::size_t i = 0, iM = 0; i < dim; i++, iM += dim) {
            for (std::size_t j = i, jM = iM - dim; j > 0; --j, jM -= dim) {
                RHS[tRHS + i] -= M[jM + i] * RHS[tRHS + j - 1];
            }
            RHS[tRHS + i] /= M[iM + i];
        }

        for (std::size_t i = dim - 1; i > 0; i--)
            for (std::size_t j = i, jM = i * dim; j < dim; j++, jM += dim) {
                RHS[tRHS + i - 1] -= M[jM + i - 1] * RHS[tRHS + j];
            }
    }

    permProdT(RHS, RHSnOfRows, p, RHSLD);
}

void
LU::forwBackSubsT(std::vector<double>& M, std::vector<double>& RHS,
        std::size_t RHSnOfRows, std::vector<std::size_t>& p, std::size_t RHSLD)
{
    forwBackSubsT(M.data(), RHS.data(), RHSnOfRows, p.data(), RHSLD);
}

void
LU::permProd(double* RHS, std::size_t RHSnOfColumns, std::size_t* p)
{
    double tmp;

    for (std::size_t i = 0; i < dim; i++)
        pcopy[i] = p[i];

    for (std::size_t i = 0; i < dim - 1; i++)
        if (pcopy[i] != i) {
            for (std::size_t j = 0; j < RHSnOfColumns; j++) {
                tmp = RHS[i * RHSnOfColumns + j];
                RHS[i * RHSnOfColumns + j] = RHS[pcopy[i] * RHSnOfColumns + j];
                RHS[pcopy[i] * RHSnOfColumns + j] = tmp;
            }
            for (std::size_t j = i; j < dim; j++)
                if (pcopy[j] == i) {
                    pcopy[j] = pcopy[i];
                    j = dim;
                }
        }
}

void
LU::permProd(std::vector<double>& RHS, std::vector<std::size_t>& p)
{
    std::size_t RHSnOfColumns = static_cast<std::size_t>(RHS.size() / dim);
    permProd(RHS.data(), RHSnOfColumns, p.data());
}

void
LU::permProdT(
        double* RHS, std::size_t RHSnOfRows, std::size_t* p, std::size_t RHSLD)
{
    for (std::size_t i = 0; i < dim; i++)
        pcopy[p[i]] = i;

    std::size_t RHSDim(dim + RHSLD);
    for (std::size_t jM = 0, jMEnd = RHSnOfRows * RHSDim; jM < jMEnd;
            jM += RHSDim) {
        std::vector<std::size_t> p2copy(pcopy);
        for (std::size_t i = 0; i < dim - 1; i++)
            if (p2copy[i] != i) {
                double aux = RHS[jM + i];
                RHS[jM + i] = RHS[jM + p2copy[i]];
                RHS[jM + p2copy[i]] = aux;
                for (std::size_t j = i; j < dim; j++)
                    if (p2copy[j] == i) {
                        p2copy[j] = p2copy[i];
                        j = dim;
                    }
            }
    }
}

void
LU::permProdT(std::vector<double>& RHS, std::size_t RHSnOfRows,
        std::vector<std::size_t>& p, std::size_t RHSLD)
{
    permProdT(RHS.data(), RHSnOfRows, p.data(), RHSLD);
}

} // namespace blots