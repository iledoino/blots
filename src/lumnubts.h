#ifndef SRC_LUMNUBTS_H
#define SRC_LUMNUBTS_H

#include "lu.h"
#include <cmath>
#include <iostream>
#include <vector>

namespace blots
{

/*
 * Linear system solver for non-uniform block tridiagonal matrices. This class
 * does the decomposition A=LU, and then uses it to solve either the linear
 * system AX=RHS or XA=RHS for X. The function blockDecomp must be called first,
 * and then blockForwSubs followed by blockBackSubs for the system AX=RHS, or
 * blockForwSubsT followed by blockBackSubsT for the system XA=RHS.
 * Alternatively, one can call the functions solve and solveT to do all of these
 * steps at once. A is a non-uniform block-tridiagonal matrix, stored using
 * three vectors: L, D and U. L contains the blocks of the lower part of A
 * (rectangular blocks), D contains the blocks of the diagonal part of A
 * (regular square bloccks), and U contains the blocks of the upper part of A
 * (rectangular blocks as well). All of the blocks are assumed to have been
 * stored in a row-wise orientation.
 */
class LUMNUBTS
{
private:
    LUMNUBTS(const LUMNUBTS&) = delete;
    LUMNUBTS& operator=(const LUMNUBTS&) = delete;

    /*
     * class used to solve the internal linear systems
     */
    LU fullLU;

    /*
     * variable that holds an upper limit for the upperCond
     */
    double upperCond, upperCondLimit;

    /*
     * variables to specify the number of blocks and the block's dimension
     */
    const std::size_t sqrBcksDim, rectBcksDim, zeroBcksDim;
    const std::size_t sqrBcksSize, rectBcksSize, zeroBcksSize;
    std::size_t sqrArraySize, rectArraySize, RHSArraySize;
    std::size_t nSqrBcks, nRectBcks;

    /*
     * Auxiliar Functions
     */
    void AddMatrixMatrixProduct(
            double* SolutionB, double* M_L, double* M_R, const double ct);

public:
    /*
     * Pointer to hold the permutation array
     */
    std::vector<std::size_t> ipvi;

    /*
     * Function to decrease the number of blocks, in case it is necessary
     */
    void NSqrBcks(const std::size_t& nOfBlocks_)
    {
        if (nOfBlocks_ <= nSqrBcks) {
            nSqrBcks = nOfBlocks_;
            nRectBcks = nOfBlocks_ - 1;
            sqrArraySize = nSqrBcks * sqrBcksSize;
            rectArraySize = nRectBcks * rectBcksSize;
            RHSArraySize = nSqrBcks * sqrBcksDim;
        }
        else
            std::cerr << "LUMNUBTS: Warning: It is not possible to increase "
                         "the number of blocks"
                      << std::endl;
    }

    /*
     * This function does the block lu decomposition, that is, if A
     * is a square matrix, then it makes A into LU, where L is block
     * lower diagonal and U is block upper diagonal
     */
    int blockDecomp(double* D, double* L, double* U);
    int blockDecomp(std::vector<double>& D, std::vector<double>& L,
            std::vector<double>& U);

    /*
     * This function does the Forward substitution necessary to solve
     * the system LY=RHS (a partial step to solve AX=RHS). nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void blockForwSubs(double* L, double* RHS, std::size_t nOfRHS = 1);
    void blockForwSubs(std::vector<double>& L, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * This function does the Forward substitution necessary to solve
     * the system YU=RHS (a partial step to solve XA=RHS). nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void
    blockForwSubsT(double* D, double* U, double* RHS, std::size_t nOfRHS = 1);
    void blockForwSubsT(std::vector<double>& D, std::vector<double>& U,
            std::vector<double>& RHS, std::size_t nOfRHS = 1);

    /*
     * This function does the Backward substitution necessary to solve
     * the system UX=Y, where X is the final solution. nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void
    blockBackSubs(double* D, double* U, double* RHS, std::size_t nOfRHS = 1);
    void blockBackSubs(std::vector<double>& D, std::vector<double>& U,
            std::vector<double>& RHS, std::size_t nOfRHS = 1);
    /*
     * This function does the Backward substitution necessary to solve
     * the system XL=Y, where X is the final solution. nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void blockBackSubsT(double* L, double* RHS, std::size_t nOfRHS = 1);
    void blockBackSubsT(std::vector<double>& L, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * Function that solves the linear system AX=RHS for X, returning 1 in case
     * it was succesfully solved
     */
    int
    solve(double* D, double* L, double* U, double* RHS, std::size_t nOfRHS = 1);
    int solve(std::vector<double>& D, std::vector<double>& L,
            std::vector<double>& U, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * Function that solves the linear system XA=RHS for X, returning 1 in case
     * it was succesfully solved
     */
    int solveT(double* D, double* L, double* U, double* RHS,
            std::size_t nOfRHS = 1);
    int solveT(std::vector<double>& D, std::vector<double>& L,
            std::vector<double>& U, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * Constructor
     */
    LUMNUBTS(const std::size_t sqrBcksDim_, const std::size_t rectBcksDim_,
            const std::size_t nSqrBcks_, const double& upperCondLimit_);
};

} // namespace blots

#endif /* SRC_LUMNUBTS_H */
