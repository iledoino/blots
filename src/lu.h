#ifndef SRC_LU_H
#define SRC_LU_H

#include <cmath>
#include <iostream>
#include <vector>

namespace blots
{

/*
 * Linear system solver that uses the LU method with pivoting
 */
class LU
{
public:
    /*
     * Dimension of the solution vector
     */
    const std::size_t dim;

private:
    /*
     * auxiliar array
     */
    std::vector<std::size_t> pcopy;

public:
    /*
     * Function that does the LU decomposition of a Matrix A stored
     * in M row by row. p is a permutation array, and it has same
     * dimension as the solution vector. zero is a limit to which a
     * value of a pivot from principal diagonal can be. upperCond
     * is a variable to hold an upper estimation of the condition
     * number of A.
     *
     * The decomposition is considered successful whenever the returned
     * value evaluates to True. Diagonal values of the decomposed matrix
     * are tested to be smaller than zero, where zero must be provided
     * by the user. In addiction, an upper estimate of the condition
     * number is provided through the constant upperCond. This estimate
     * is usually good for matrices close to being "equilibrated",
     * according to the definition in
     * https://doi.org/10.1080/07468342.1995.11973657.
     */
    int
    decomp(double* M, std::size_t* p, const double& zero, double& upperCond);
    int decomp(std::vector<double>& M, std::vector<std::size_t>& p,
            double& upperCond, double zero = 1.0e-16);

    /*
     * Function that solves AX=B, where A is stored in M and B is
     * stored in RHS, both row by row. RHSnOfColumns is the number of
     * columns stored in RHS. p is a permutation array, and it has same
     * dimension as the solution vector. Notice that p has to be the
     * same used on decomp function.
     */
    void forwBackSubs(
            double* M, double* RHS, std::size_t RHSnOfColumns, std::size_t* p);
    void forwBackSubs(std::vector<double>& M, std::vector<double>& RHS,
            std::vector<std::size_t>& p);

    /*
     * Function that solves XA=B, where A is stored in M and B is
     * stored in RHS, both row by row. RHSnOfRows is the number of
     * rows stored in RHS. p is a permutation array, and it has same
     * dimension as the solution vector. Notice that p has to be the
     * same used on decomp function. RHSLD is the leading dimension
     * of RHS (the number of entries between the last column in a row
     * to first column of the next)
     */
    void forwBackSubsT(double* M, double* RHS, std::size_t RHSnOfRows,
            std::size_t* p, std::size_t RHSLD = 0);
    void forwBackSubsT(std::vector<double>& M, std::vector<double>& RHS,
            std::size_t RHSnOfRows, std::vector<std::size_t>& p,
            std::size_t RHSLD = 0);

    /*
     * Functions used to multiply the permutation array by a matrix
     */
    void permProd(double* RHS, std::size_t RHSnOfColumns, std::size_t* p);
    void permProd(std::vector<double>& RHS, std::vector<std::size_t>& p);

    void permProdT(double* RHS, std::size_t RHSnOfRows, std::size_t* p,
            std::size_t RHSLD = 0);
    void permProdT(std::vector<double>& RHS, std::size_t RHSnOfRows,
            std::vector<std::size_t>& p, std::size_t RHSLD = 0);

    /*
     * Constructor
     */
    LU(const std::size_t& dim);
};

} // namespace blots

#endif /* SRC_LU_H */
