#ifndef SRC_LUMBTS_H
#define SRC_LUMBTS_H

#include "lu.h"
#include <cmath>
#include <iostream>
#include <vector>

namespace blots
{

/*
 * Linear system solver for uniform block tridiagonal matrices. This class does
 * the decomposition A=LU, and then uses it to solve either the linear system
 * AX=RHS or XA=RHS for X. The function blockDecomp must be called first, and
 * then blockForwSubs followed by blockBackSubs for the system AX=RHS, or
 * blockForwSubsT followed by blockBackSubsT for the system XA=RHS.
 * Alternatively, one can call the functions solve and solveT to do all of these
 * steps at once. A is a uniform block-tridiagonal matrix, stored using three
 * vectors: L, D and U. L contains the blocks of the lower part of A, D contains
 * the blocks of the diagonal part of A, and U contains the blocks of the upper
 * part of A. All of the blocks are assumed to have been stored in a row-wise
 * orientation.
 */
class LUMBTS
{
private:
    LUMBTS(const LUMBTS&) = delete;
    LUMBTS& operator=(const LUMBTS&) = delete;

    /*
     * class used to solve the internal linear systems
     */
    LU fullLU;

    /*
     * variable that holds an upper limit for the upperCond
     */
    double upperCond, upperCondLimit;

    /*
     * variables to specify the number of blocks and the block's dimension
     */
    std::size_t nOfBlocks;
    const std::size_t blockDim, blockSize;

public:
    /*
     * Pointer to hold the permutation array
     */
    std::vector<std::size_t> ipvi;

    /*
     * Function to decrease the number of blocks, in case it is necessary
     */
    void NOfBlocks(const std::size_t& nOfBlocks_)
    {
        if (nOfBlocks_ <= nOfBlocks)
            nOfBlocks = nOfBlocks_;
        else
            std::cerr << "LUMBTS: Warning: It is not possible to increase "
                         "the number of blocks"
                      << std::endl;
    }

    /*
     * This function does the block lu decomposition, that is, if A
     * is a square matrix, then it makes A into LU, where L is block
     * lower diagonal and U is block upper diagonal
     */
    int blockDecomp(double* D, double* L, double* U);
    int blockDecomp(std::vector<double>& D, std::vector<double>& L,
            std::vector<double>& U);

    /*
     * This function does the Forward substitution necessary to solve
     * the system LY=RHS (a partial step to solve AX=RHS). nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void blockForwSubs(double* L, double* RHS, std::size_t nOfRHS = 1);
    void blockForwSubs(std::vector<double>& L, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * This function does the Forward substitution necessary to solve
     * the system YU=RHS (a partial step to solve XA=RHS). nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void
    blockForwSubsT(double* D, double* U, double* RHS, std::size_t nOfRHS = 1);
    void blockForwSubsT(std::vector<double>& D, std::vector<double>& U,
            std::vector<double>& RHS, std::size_t nOfRHS = 1);

    /*
     * This function does the Backward substitution necessary to solve
     * the system UX=Y, where X is the final solution. nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void
    blockBackSubs(double* D, double* U, double* RHS, std::size_t nOfRHS = 1);
    void blockBackSubs(std::vector<double>& D, std::vector<double>& U,
            std::vector<double>& RHS, std::size_t nOfRHS = 1);

    /*
     * This function does the Backward substitution necessary to solve
     * the system XL=Y, where X is the final solution. nOfRHS is the number
     * of right hand sides stored in RHS. The result of the operation is stored
     * in RHS, and all matrices are assumed to be stored in a row-wise
     * orientation.
     */
    void blockBackSubsT(double* L, double* RHS, std::size_t nOfRHS = 1);
    void blockBackSubsT(std::vector<double>& L, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * Function that solves the linear system AX=RHS for X,
     * returning 1 in case it was succesfully solved
     */
    int
    solve(double* D, double* L, double* U, double* RHS, std::size_t nOfRHS = 1);
    int solve(std::vector<double>& D, std::vector<double>& L,
            std::vector<double>& U, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * Function that solves the linear system XA=RHS for X,
     * returning 1 in case it was succesfully solved
     */
    int
    solveT(double* D, double* L, double* U, double* RHS, std::size_t nOfRHS = 1);
    int solveT(std::vector<double>& D, std::vector<double>& L,
            std::vector<double>& U, std::vector<double>& RHS,
            std::size_t nOfRHS = 1);

    /*
     * Constructor
     */
    LUMBTS(std::size_t nOfBlocks_, std::size_t blockDim_,
            const double& upperCondLimit_);
};

} // namespace blots

#endif /* SRC_LUMBTS_H */
