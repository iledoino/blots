#ifndef SRC_NUBTS_H
#define SRC_NUBTS_H

/*
 * This software was created by Ismael de Souza Ledoino
 *<ismael.sledoino@gmail.com>, with supervision of Mary Pugh
 *<mpugh@math.toronto.edu> (University of Toronto) and
 * Dan Marchesi <marchesi@impa.br> (Instituto de Matematica Pura e Aplicada).
 * Comments and suggestions, or even questions, please email the developer.
 */
#include "lumbts.h"
#include "lumnubts.h"
#include <cmath>
#include <iostream>

namespace blots
{

/*
 * Non-uniform block tridiagonal system solver
 * PS: This solver wraps other solvers specific for when the blocks are uniform
 */
class NUBTS
{
private:
    NUBTS(const NUBTS&) = delete;
    NUBTS& operator=(const NUBTS&) = delete;

public:
    /*
     * data that holds the matrix and its size
     */
    const std::size_t sqrBcksDim, rectBcksDim, zeroBcksDim;
    const std::size_t sqrBcksSize, rectBcksSize, zeroBcksSize;
    const std::size_t sqrArraySize, rectArraySize, RHSArraySize;
    const std::size_t nSqrBcks, nRectBcks;
    double *mDiag, *mLower, *mUpper, *RHS;
    double zero, upperCond, upperCondMax;

    /*
     * Extra Data for child classes
     * OBS: The user needs to modify these data in child classes
     */
    double* leftBdryExtraBck;
    double* rightBdryExtraBck;

    /*
     * The function solve() executes Gaussian Elimination plus
     * Backward Elimination, giving as an output an integer that
     * tells whether the system was solved succesfully or not.
     */
    virtual int solve() { return (this->*solveLinearSystem)(); }

protected:
    /*
     * This class is used only if the rectangular blocks dimension is 0
     */
    LUMBTS* blockLU;
    LUMNUBTS* nuBlockLU;

    /*
     * solves the linear system in case the rectangular blocks
     * dimension is not 0
     */
    virtual int solveNUBTS();

    /*
     * solves the linear system in case the rectangular blocks
     * dimension is 0
     */
    virtual int solveLUMBTS();

    /*
     * Pointer to function that defines which solve function to call
     */
    typedef int (NUBTS::*solveLSFunction)();
    solveLSFunction solveLinearSystem;

public:
    /*
     * Constructor
     *  - squareBlocksDimension       --> dimension of the blocks that are at
     *the diagonal of the matrix
     *  - rectangularBlocksDimension  --> number of rows of the blocks that are
     *at the right and left side of the diagonal
     *  - numberOfSquareBlocks        --> total number of square blocks inside
     *the matrix
     *  - zeroValue                   --> a positive value which is the
     *tolerance for how small the norm of the pivots can get
     *  - upperCondMaximumValue --> a big an positive value which is the
     *tolerance for how big the condition number of the matrix can get (an
     *approximation of the condition number is done, and the user is informed in
     *case of tolerance reached).
     */
    NUBTS(std::size_t squareBlocksDimension,
            std::size_t rectangularBlocksDimension,
            std::size_t numberOfSquareBlocks, double zeroValue = 1.0e-16,
            double upperCondMaximumValue = 1000);

    /*
     * Destructors
     */
    virtual ~NUBTS();
};

/*
 * The class NUBCS acts as NUBTS, except that it solves linear systems
 * generated from non-uniform Block Circular matrices, which are tipically
 * found when using periodic boundary conditions to solve PDEs numerically.
 */
class NUBCS : public NUBTS
{
private:
    NUBCS(const NUBCS&) = delete;
    NUBCS& operator=(const NUBCS&);

protected:
    /*
     * Extra data for the extra RHS (which includes the two blocks)
     * OBS: The user does not have to modify these data
     */
    double* extraRightRHS;

    /*
     * Auxiliar Functions
     */
    void FillextraRHS();
    void CalculatesSmallSystem(double* LN, double* LNminus1, double* UN,
            double* UNminus1, double* mDiagN, double* RHSN, double* RHSNminus1,
            double* mRHSN);
    static void
    AddMatrixProduct(double* Mtr1, double* Mtr2, double* RHS, const double& ct,
            const std::size_t& m, const std::size_t& n, const std::size_t& p);

    /*
     * solves the linear system in case the rectangular blocks
     * dimension is not 0
     */
    int solveNUBTS() override;

    /*
     * solves the linear system in case the rectangular blocks
     * dimension is 0
     */
    int solveLUMBTS() override;

public:
    /*
     * Principal function, overriden from base class
     */
    virtual int solve() override;

    /*
     * Constructor
     * The parameters for this construcor are the same ones used for
     * NUBTS. Please have a look to NUBTS class.
     */
    NUBCS(std::size_t squareBlocksDimension,
            std::size_t rectangularBlocksDimension,
            std::size_t numberOfSquareBlocks, double zeroValue = 1.0e-16,
            double upperCondMaximumValue = 1000);

    /*
     * Destructor
     */
    virtual ~NUBCS();
};

/*
 * The class NUSBTS solves linear systems where the first and last rows
 * of blocks have actually three blocks rather than only two
 */
class NUSBTS : public NUBCS
{
private:
    NUSBTS(const NUSBTS&) = delete;
    NUSBTS& operator=(const NUSBTS&) = delete;

public:
    /*
     * Principal function, overriden from base class
     */
    virtual int solve() override;

    /*
     * Constructor
     */
    NUSBTS(std::size_t squareBlocksDimension,
            std::size_t rectangularBlocksDimension,
            std::size_t numberOfSquareBlocks, double zeroValue = 1.0e-16,
            double upperCondMaximumValue = 1000);

    /*
     * Destructor
     */
    virtual ~NUSBTS();

private:
    /*
     * Extra data for the extra RHSs (which include the two blocks)
     * OBS: The user does not have to modify these data
     */
    double* extraLeftRHS;
    double* auxBck;

    /*
     * Functions that are overriden from base class
     */
    void FillextraRHS();
    void CalculatesSmallSystem(double* A_1, double* theta, double* gamma_1,
            double* phi_1, double* A_N, double* omega, double* gamma_2,
            double* phi_2, double* b_1, double* b_N, double* overline_b_1,
            double* overline_b_2);
    int solveSmallSystem(double* A_1, double* A_N, double* theta, double* omega,
            double* b_1, double* b_N);

    /*
     * solves the linear system in case the rectangular blocks
     * dimension is not 0
     */
    int solveNUBTS() override;

    /*
     * solves the linear system in case the rectangular blocks
     * dimension is 0
     */
    int solveLUMBTS() override;
};

} // namespace blots

#endif /* SRC_NUBTS_H */
