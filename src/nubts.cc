/*
 * This software was created by Ismael de Souza Ledoino
 *<ismael.sledoino@gmail.com>,
 * with supervision of Mary Pugh <mpugh@math.toronto.edu> (University of
 *Toronto) and
 * Dan Marchesi <marchesi@impa.br> (Instituto de Matematica Pura e Aplicada).
 * Commentaries and suggestions, or even questions, please email the developer.
 */
#include "nubts.h"
#include <iomanip>

namespace blots
{

/*
 * FUNCTIONS FOR CLASS NUBTS
 */

NUBTS::NUBTS(std::size_t squareBlocksDimension,
        std::size_t rectangularBlocksDimension,
        std::size_t numberOfSquareBlocks, double zeroValue,
        double upperCondMaximumValue)
    : sqrBcksDim(squareBlocksDimension)
    , rectBcksDim(rectangularBlocksDimension)
    , zeroBcksDim(squareBlocksDimension - rectangularBlocksDimension)
    , sqrBcksSize(squareBlocksDimension * squareBlocksDimension)
    , rectBcksSize(squareBlocksDimension * rectangularBlocksDimension)
    , zeroBcksSize((squareBlocksDimension - rectangularBlocksDimension)
              * squareBlocksDimension)
    , sqrArraySize(numberOfSquareBlocks * squareBlocksDimension
              * squareBlocksDimension)
    , rectArraySize((numberOfSquareBlocks - 1) * rectangularBlocksDimension
              * squareBlocksDimension)
    , RHSArraySize(numberOfSquareBlocks * squareBlocksDimension)
    , nSqrBcks(numberOfSquareBlocks)
    , nRectBcks(numberOfSquareBlocks - 1)
    , mDiag(nullptr)
    , mLower(nullptr)
    , mUpper(nullptr)
    , RHS(nullptr)
    , zero(zeroValue)
    , upperCond(1.)
    , upperCondMax(upperCondMaximumValue)
    , leftBdryExtraBck(nullptr)
    , rightBdryExtraBck(nullptr)
    , blockLU(nullptr)
    , nuBlockLU(nullptr)
    , solveLinearSystem(nullptr)
{
    mDiag = new double[sqrArraySize];
    mLower = new double[rectArraySize];
    mUpper = new double[rectArraySize];
    RHS = new double[RHSArraySize];

    leftBdryExtraBck = new double[rectBcksSize];
    rightBdryExtraBck = new double[rectBcksSize];

    if (zeroBcksDim > 0) {
        nuBlockLU
                = new LUMNUBTS(sqrBcksDim, rectBcksDim, nSqrBcks, upperCondMax);

        solveLinearSystem = &NUBTS::solveNUBTS;
    }
    else {
        blockLU = new LUMBTS(nSqrBcks, sqrBcksDim, upperCondMax);

        solveLinearSystem = &NUBTS::solveLUMBTS;
    }
}

NUBTS::~NUBTS()
{
    delete[] mDiag;
    delete[] mLower;
    delete[] mUpper;
    delete[] RHS;

    delete[] leftBdryExtraBck;
    delete[] rightBdryExtraBck;

    if (zeroBcksDim > 0)
        delete nuBlockLU;
    else
        delete blockLU;
}

int
NUBTS::solveNUBTS()
{
    auto returned_value = nuBlockLU->solve(mDiag, mLower, mUpper, RHS);

    if (returned_value != 1)
        return 0;
    else
        return 1;
}

int
NUBTS::solveLUMBTS()
{
    auto returned_value = blockLU->solve(mDiag, mLower, mUpper, RHS);

    if (returned_value != 1)
        return 0;
    else
        return 1;
}

/*
 * FUNCTIONS FOR CLASS NUBCS
 */

NUBCS::NUBCS(std::size_t squareBlocksDimension,
        std::size_t rectangularBlocksDimension,
        std::size_t numberOfSquareBlocks, double zeroValue,
        double upperCondMaximumValue)
    : NUBTS(squareBlocksDimension, rectangularBlocksDimension,
            numberOfSquareBlocks, zeroValue, upperCondMaximumValue)
    , extraRightRHS(nullptr)
{
    extraRightRHS = new double[sqrArraySize + rectBcksSize];

    delete[] leftBdryExtraBck;
    delete[] rightBdryExtraBck;

    leftBdryExtraBck = extraRightRHS + zeroBcksSize;
    rightBdryExtraBck = extraRightRHS + sqrArraySize;
}

NUBCS::~NUBCS()
{
    leftBdryExtraBck = new double[2];
    rightBdryExtraBck = new double[2];

    delete[] extraRightRHS;
}

void
NUBCS::FillextraRHS()
{
    /*
     * Fill first zeroBcksDim rows with zeros
     */
    for (std::size_t i = 0; i != zeroBcksSize; ++i)
        extraRightRHS[i] = 0.0;

    /*
     * Rows zeroBcksDim to sqrBcksDim-1 are supposed to be filled by the user
     */

    /*
     * Rows between the left extra non uniform block and the last U block
     * are also filled with zeros
     */
    for (std::size_t i = sqrBcksSize,
                     j = sqrArraySize - (sqrBcksSize + rectBcksSize);
            i != j; ++i)
        extraRightRHS[i] = 0.0;

    /*
     * The last U block is copied to extraRightRHS
     */
    for (std::size_t i = sqrArraySize - (sqrBcksSize + rectBcksSize),
                     j = rectArraySize - rectBcksSize,
                     k = sqrArraySize - sqrBcksSize;
            i != k; ++i, ++j)
        extraRightRHS[i] = mUpper[j];

    /*
     * The last D block is copied to extraRightRHS
     */
    for (std::size_t i = sqrArraySize - sqrBcksSize; i != sqrArraySize; ++i)
        extraRightRHS[i] = mDiag[i];

    /*
     * Rows for the right extra non uniform block are supposed to be filled by
     * the user
     */
}

int
NUBCS::solveNUBTS()
{
    nuBlockLU->NSqrBcks(nSqrBcks - 1);
    auto returned_value = nuBlockLU->blockDecomp(mDiag, mLower, mUpper);
    nuBlockLU->blockForwSubs(mLower, RHS);
    nuBlockLU->blockForwSubs(mLower, extraRightRHS, sqrBcksDim);

    nuBlockLU->blockBackSubs(mDiag, mUpper, RHS);
    nuBlockLU->blockBackSubs(mDiag, mUpper, extraRightRHS, sqrBcksDim);

    if (returned_value != 1)
        return 0;
    else
        return 1;
}

int
NUBCS::solveLUMBTS()
{
    blockLU->NOfBlocks(nSqrBcks - 1);
    auto returned_value = blockLU->blockDecomp(mDiag, mLower, mUpper);
    blockLU->blockForwSubs(mLower, RHS);
    blockLU->blockForwSubs(mLower, extraRightRHS, sqrBcksDim);

    blockLU->blockBackSubs(mDiag, mUpper, RHS);
    blockLU->blockBackSubs(mDiag, mUpper, extraRightRHS, sqrBcksDim);

    if (returned_value != 1)
        return 0;
    else
        return 1;
}

int
NUBCS::solve()
{
    FillextraRHS();
    int eliminationResult = (this->*solveLinearSystem)();

    if (eliminationResult > 0) {
        CalculatesSmallSystem(rightBdryExtraBck,
                mLower + rectArraySize - rectBcksSize, extraRightRHS,
                extraRightRHS + sqrArraySize - 2 * sqrBcksSize,
                mDiag + sqrArraySize - sqrBcksSize, RHS,
                RHS + RHSArraySize - 2 * sqrBcksDim,
                RHS + RHSArraySize - sqrBcksDim);

        LU smallSystem(sqrBcksDim);

        std::size_t* p = new std::size_t[sqrBcksDim];
        eliminationResult = smallSystem.decomp(
                mDiag + sqrArraySize - sqrBcksSize, p, zero, upperCond);
        smallSystem.forwBackSubs(mDiag + sqrArraySize - sqrBcksSize,
                RHS + RHSArraySize - sqrBcksDim, 1, p);
        delete[] p;

        if (eliminationResult != 0)
            AddMatrixProduct(extraRightRHS, RHS + RHSArraySize - sqrBcksDim,
                    RHS, -1., RHSArraySize - sqrBcksDim, sqrBcksDim, 1);
        else
            std::cerr << "NUBCS::solve: Error: could not solve the last "
                         "linear system.\n";

        return eliminationResult;
    }
    else
        return eliminationResult;
}

void
NUBCS::AddMatrixProduct(double* Mtr1, double* Mtr2, double* RHS,
        const double& ct, const std::size_t& m, const std::size_t& n,
        const std::size_t& p)
{
    double tmp;

    for (std::size_t kM1 = 0, kRHS = 0; kM1 < m * n; kM1 += n, kRHS += p)
        for (std::size_t i = 0, iM2 = 0; i < n; i++, iM2 += p) {
            tmp = ct * Mtr1[kM1 + i];
            for (std::size_t j = 0; j < p; j++)
                RHS[kRHS + j] += tmp * Mtr2[iM2 + j];
        }
}

void
NUBCS::CalculatesSmallSystem(double* LN, double* LNminus1, double* UN,
        double* UNminus1, double* mDiagN, double* RHSN, double* RHSNminus1,
        double* mRHSN)
{
    double tmp1, tmp2;

    for (std::size_t i = 0; i != rectBcksDim; ++i)
        for (std::size_t j = 0; j != sqrBcksDim; ++j) {
            tmp1 = LN[i * sqrBcksDim + j];
            tmp2 = LNminus1[i * sqrBcksDim + j];

            for (std::size_t k = 0; k != sqrBcksDim; ++k)
                mDiagN[(i + zeroBcksDim) * sqrBcksDim + k]
                        -= tmp1 * UN[j * sqrBcksDim + k]
                        + tmp2 * UNminus1[j * sqrBcksDim + k];
        }

    for (std::size_t i = 0; i != rectBcksDim; ++i)
        for (std::size_t j = 0; j != sqrBcksDim; ++j)
            mRHSN[i + zeroBcksDim] -= LN[i * sqrBcksDim + j] * RHSN[j]
                    + LNminus1[i * sqrBcksDim + j] * RHSNminus1[j];
}

/*
 * FUNCTIONS FOR CLASS NUSBTS
 */

NUSBTS::NUSBTS(std::size_t squareBlocksDimension,
        std::size_t rectangularBlocksDimension,
        std::size_t numberOfSquareBlocks, double zeroValue,
        double upperCondMaximumValue)
    : NUBCS(squareBlocksDimension, rectangularBlocksDimension,
            numberOfSquareBlocks, zeroValue, upperCondMaximumValue)
    , extraLeftRHS(nullptr)
    , auxBck(nullptr)
{
    extraLeftRHS = new double[sqrArraySize - sqrBcksSize];
    auxBck = new double[rectBcksSize];

    leftBdryExtraBck = extraRightRHS + zeroBcksSize;
    rightBdryExtraBck = extraLeftRHS + zeroBcksSize;
}

NUSBTS::~NUSBTS()
{
    delete[] extraLeftRHS;
    delete[] auxBck;
}

void
NUSBTS::FillextraRHS()
{
    // PIECE OF CODE THAT FILLS extraRightRHS

    /*
     * Fill first zeroBcksDim rows with zeros
     */
    for (std::size_t i = 0; i != zeroBcksSize; ++i)
        extraRightRHS[i] = 0.0;

    /*
     * Rows zeroBcksDim to sqrBcksDim-1 are supposed to be filled by the user
     */

    /*
     * Rows between the left extra non uniform block and the last U block
     * are also filled with zeros
     */
    for (std::size_t i = sqrBcksSize;
            i != sqrArraySize - (sqrBcksSize + rectBcksSize); ++i)
        extraRightRHS[i] = 0.0;

    /*
     * The last U block is copied to extraRightRHS
     */
    for (std::size_t i = sqrArraySize - (sqrBcksSize + rectBcksSize),
                     j = rectArraySize - rectBcksSize;
            i != sqrArraySize - sqrBcksSize; ++i, ++j)
        extraRightRHS[i] = mUpper[j];

    // PIECE OF CODE THAT FILLS extraLeftRHS

    /*
     * Fill first zeroBcksDim rows with zeros
     */
    for (std::size_t i = 0; i < zeroBcksSize; ++i)
        extraLeftRHS[i] = 0.0;

    /*
     * Rows zeroBcksDim to sqrBcksDim-1 are supposed to be filled by the user
     */

    /*
     * Fill zeroBcksDim more rows with zeros
     */
    for (std::size_t i = sqrBcksSize; i < sqrBcksSize + zeroBcksSize; ++i)
        extraLeftRHS[i] = 0.0;

    /*
     * Rows that are a copy of the first lower block
     */
    for (std::size_t i = sqrBcksSize + zeroBcksSize, j = 0; j < rectBcksSize;
            ++i, ++j)
        extraLeftRHS[i] = mLower[j];

    for (std::size_t i = 2 * sqrBcksSize; i < sqrArraySize - sqrBcksSize; ++i)
        extraLeftRHS[i] = 0.0;
}

void
NUSBTS::CalculatesSmallSystem(double* A_1, double* theta, double* gamma_1,
        double* phi_1, double* A_N, double* omega, double* gamma_2,
        double* phi_2, double* b_1, double* b_N, double* overline_b_1,
        double* overline_b_2)
{
    /*
     * Calculates A_1-\theta \overline{ \gamma }
     */
    NUBCS::AddMatrixProduct(theta, gamma_1, A_1 + zeroBcksSize, -1.0,
            rectBcksDim, sqrBcksDim, sqrBcksDim);
    NUBCS::AddMatrixProduct(leftBdryExtraBck, gamma_1 + sqrBcksSize,
            A_1 + zeroBcksSize, -1.0, rectBcksDim, sqrBcksDim, sqrBcksDim);

    /*
     * Calculates b_1-\theta \overline{ b^{'} }
     */
    NUBCS::AddMatrixProduct(theta, overline_b_1, b_1 + zeroBcksDim, -1.0,
            rectBcksDim, sqrBcksDim, 1);
    NUBCS::AddMatrixProduct(leftBdryExtraBck, overline_b_1 + sqrBcksDim,
            b_1 + zeroBcksDim, -1.0, rectBcksDim, sqrBcksDim, 1);

    /*
     * Calculates -\theta \overline{ \phi } and stores this product into \theta
     */
    for (std::size_t i = 0; i != rectBcksSize; ++i)
        auxBck[i] = 0.0;
    NUBCS::AddMatrixProduct(
            theta, phi_1, auxBck, -1.0, rectBcksDim, sqrBcksDim, sqrBcksDim);
    NUBCS::AddMatrixProduct(leftBdryExtraBck, phi_1 + sqrBcksSize, auxBck, -1.0,
            rectBcksDim, sqrBcksDim, sqrBcksDim);
    for (std::size_t i = 0; i != rectBcksSize; ++i)
        theta[i] = auxBck[i];

    /*
     * Calculates A_N-\omega \overline{ \phi }
     */
    NUBCS::AddMatrixProduct(omega, phi_2 + sqrBcksSize, A_N + zeroBcksSize,
            -1.0, rectBcksDim, sqrBcksDim, sqrBcksDim);
    NUBCS::AddMatrixProduct(rightBdryExtraBck, phi_2, A_N + zeroBcksSize, -1.0,
            rectBcksDim, sqrBcksDim, sqrBcksDim);

    /*
     * Calculates b_N-\omega \overline{ b^{'} }
     */
    NUBCS::AddMatrixProduct(omega, overline_b_2 + sqrBcksDim, b_N + zeroBcksDim,
            -1.0, rectBcksDim, sqrBcksDim, 1);
    NUBCS::AddMatrixProduct(rightBdryExtraBck, overline_b_2, b_N + zeroBcksDim,
            -1.0, rectBcksDim, sqrBcksDim, 1);

    /*
     * Calculates -\omega \overline{ \gamma } and stores this product into
     * \omega
     */
    for (std::size_t i = 0; i != rectBcksSize; ++i)
        auxBck[i] = 0.0;
    NUBCS::AddMatrixProduct(omega, gamma_2 + sqrBcksSize, auxBck, -1.0,
            rectBcksDim, sqrBcksDim, sqrBcksDim);
    NUBCS::AddMatrixProduct(rightBdryExtraBck, gamma_2, auxBck, -1.0,
            rectBcksDim, sqrBcksDim, sqrBcksDim);
    for (std::size_t i = 0; i != rectBcksSize; ++i)
        omega[i] = auxBck[i];
}

int
NUSBTS::solveSmallSystem(double* A_1, double* A_N, double* theta, double* omega,
        double* b_1, double* b_N)
{
    double* D_sm = new double[2 * sqrBcksSize];
    double* L_sm = new double[2 * rectBcksSize];
    double* U_sm = new double[2 * rectBcksSize];
    double* RHS_sm = new double[2 * sqrBcksDim];
    for (std::size_t i = 0, j = sqrBcksSize; i < sqrBcksSize; ++i, ++j) {
        D_sm[i] = A_1[i];
        D_sm[j] = A_N[i];
    }
    for (std::size_t i = 0; i < rectBcksSize; ++i) {
        L_sm[i] = omega[i];
        U_sm[i] = theta[i];
    }
    for (std::size_t i = 0, j = sqrBcksDim; i < sqrBcksDim; ++i, ++j) {
        RHS_sm[i] = b_1[i];
        RHS_sm[j] = b_N[i];
    }
//#define PRINT_SMALL_SYSTEM_MATLAB_NUSBTS
#ifdef PRINT_SMALL_SYSTEM_MATLAB_NUSBTS
    std::cerr << std::scientific << std::setprecision(16);
    std::cerr << "dtriplets = zeros(" << 2 * sqrBcksSize << ",3);\n";
    std::cerr << "utriplets = zeros(" << rectBcksSize << ",3);\n";
    std::cerr << "ltriplets = zeros(" << rectBcksSize << ",3);\n";
    std::cerr << "rhs = zeros(" << 2 * sqrBcksDim << ",1);\n";
    std::cerr << "\n% block diagonal entries\n";
    for (std::size_t i = 0, ii = 1; i != 2; ++i)
        for (std::size_t j = 0; j != sqrBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "dtriplets(" << ii << ", :) = ["
                          << 1 + i * sqrBcksDim + j << ", "
                          << 1 + i * sqrBcksDim + k << ", "
                          << D_sm[i * sqrBcksSize + j * sqrBcksDim + k]
                          << "];\n";
    std::cerr << "\n% block lower entries\n";
    for (std::size_t i = 0, ii = 1; i != 1; ++i)
        for (std::size_t j = 0; j != rectBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "ltriplets(" << ii << ", :) = ["
                          << 1 + (i + 1) * sqrBcksDim + zeroBcksDim + j << ", "
                          << 1 + i * sqrBcksDim + k << ", "
                          << L_sm[i * rectBcksSize + j * sqrBcksDim + k]
                          << "];\n";
    std::cerr << "\n% block upper entries\n";
    for (std::size_t i = 0, ii = 1; i != 1; ++i)
        for (std::size_t j = 0; j != rectBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "utriplets(" << ii << ", :) = ["
                          << 1 + i * sqrBcksDim + zeroBcksDim + j << ", "
                          << 1 + (i + 1) * sqrBcksDim + k << ", "
                          << U_sm[i * rectBcksSize + j * sqrBcksDim + k]
                          << "];\n";
    std::cerr << "\n% rhs entries\n";
    std::cerr << "rhs(:,1) = [";
    for (std::size_t i = 0; i != 2 * sqrBcksDim; i++)
        std::cerr << RHS_sm[i] << std::endl;
    std::cerr << "];\n";
    std::cerr << "\n% bulding the sparse matrix\n";
    std::cerr << "alltriplets = [dtriplets ; utriplets ; ltriplets];\n";
    std::cerr << "S = "
                 "sparse(alltriplets(:,1),alltriplets(:,2),alltriplets(:,3));"
                 "\n\n\n";
#endif

    LUMBTS smallSystem_un(2, sqrBcksDim, upperCondMax);
    LUMNUBTS smallSystem_nu(sqrBcksDim, rectBcksDim, 2, upperCondMax);
    int returned_value = (sqrBcksDim == rectBcksDim ?
                    smallSystem_un.solve(D_sm, L_sm, U_sm, RHS_sm) :
                    smallSystem_nu.solve(D_sm, L_sm, U_sm, RHS_sm));

#ifdef PRINT_SMALL_SYSTEM_MATLAB_NUSBTS
    std::cerr << "sol = zeros(" << 2 * sqrBcksDim << ",1);\n";
    std::cerr << "sol(:,1) = [";
    for (std::size_t i = 0; i != 2 * sqrBcksDim; i++)
        std::cerr << RHS_sm[i] << std::endl;
    std::cerr << "];\n";
    exit(1);
#endif

    for (std::size_t i = 0, j = sqrBcksDim; i < sqrBcksDim; ++i, ++j) {
        b_1[i] = RHS_sm[i];
        b_N[i] = RHS_sm[j];
    }

    delete[] D_sm;
    delete[] L_sm;
    delete[] U_sm;
    delete[] RHS_sm;

    return returned_value;
}

int
NUSBTS::solveNUBTS()
{
    nuBlockLU->NSqrBcks(nSqrBcks - 2);
    auto returned_value = nuBlockLU->solve(mDiag + sqrBcksSize,
            mLower + rectBcksSize, mUpper + rectBcksSize, RHS + sqrBcksDim);

    nuBlockLU->blockForwSubs(
            mLower + rectBcksSize, extraLeftRHS + sqrBcksSize, sqrBcksDim);
    nuBlockLU->blockForwSubs(
            mLower + rectBcksSize, extraRightRHS + sqrBcksSize, sqrBcksDim);

    nuBlockLU->blockBackSubs(mDiag + sqrBcksSize, mUpper + rectBcksSize,
            extraLeftRHS + sqrBcksSize, sqrBcksDim);
    nuBlockLU->blockBackSubs(mDiag + sqrBcksSize, mUpper + rectBcksSize,
            extraRightRHS + sqrBcksSize, sqrBcksDim);

    if (returned_value != 1)
        return 0;
    else
        return 1;
}

int
NUSBTS::solveLUMBTS()
{
    blockLU->NOfBlocks(nSqrBcks - 2);
//#define PRINT_BIG_SYSTEM_MATLAB_NUSBTS
#ifdef PRINT_BIG_SYSTEM_MATLAB_NUSBTS
    std::cerr << std::scientific << std::setprecision(16);
    std::cerr << "dtriplets = zeros(" << (nSqrBcks - 2) * sqrBcksSize
              << ",3);\n";
    std::cerr << "utriplets = zeros(" << (nSqrBcks - 3) * rectBcksSize
              << ",3);\n";
    std::cerr << "ltriplets = zeros(" << (nSqrBcks - 3) * rectBcksSize
              << ",3);\n";
    std::cerr << "rhs = zeros(" << (nSqrBcks - 2) * sqrBcksDim << ","
              << 1 + 2 * sqrBcksDim << ");\n";
    std::cerr << "\n% block diagonal entries\n";
    for (std::size_t i = 0, ii = 1; i != nSqrBcks - 2; ++i)
        for (std::size_t j = 0; j != sqrBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "dtriplets(" << ii << ", :) = ["
                          << 1 + i * sqrBcksDim + j << ", "
                          << 1 + i * sqrBcksDim + k << ", "
                          << mDiag[i * sqrBcksSize + j * sqrBcksDim + k
                                     + sqrBcksSize]
                          << "];\n";
    std::cerr << "\n% block lower entries\n";
    for (std::size_t i = 0, ii = 1; i != nRectBcks - 2; ++i)
        for (std::size_t j = 0; j != rectBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "ltriplets(" << ii << ", :) = ["
                          << 1 + (i + 1) * sqrBcksDim + zeroBcksDim + j << ", "
                          << 1 + i * sqrBcksDim + k << ", "
                          << mLower[i * rectBcksSize + j * sqrBcksDim + k
                                     + rectBcksSize]
                          << "];\n";
    std::cerr << "\n% block upper entries\n";
    for (std::size_t i = 0, ii = 1; i != nRectBcks - 2; ++i)
        for (std::size_t j = 0; j != rectBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "utriplets(" << ii << ", :) = ["
                          << 1 + i * sqrBcksDim + zeroBcksDim + j << ", "
                          << 1 + (i + 1) * sqrBcksDim + k << ", "
                          << mUpper[i * rectBcksSize + j * sqrBcksDim + k
                                     + rectBcksSize]
                          << "];\n";
    std::cerr << "\n% rhs entries\n";
    std::cerr << "rhs(:,:) = [";
    for (std::size_t i = 0; i != (nSqrBcks - 2) * sqrBcksDim; i++) {
        std::cerr << RHS[i + sqrBcksDim] << " ";
        for (std::size_t j = 0; j < sqrBcksDim; ++j)
            std::cerr << extraLeftRHS[sqrBcksSize + i * sqrBcksDim + j] << " ";
        for (std::size_t j = 0; j < sqrBcksDim; ++j)
            std::cerr << extraRightRHS[sqrBcksSize + i * sqrBcksDim + j] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];\n";
    std::cerr << "\n% bulding the sparse matrix\n";
    std::cerr << "alltriplets = [dtriplets ; utriplets ; ltriplets];\n";
    std::cerr << "S = "
                 "sparse(alltriplets(:,1),alltriplets(:,2),alltriplets(:,3));"
                 "\n\n\n";
#endif
    auto returned_value = blockLU->blockDecomp(
            mDiag + sqrBcksSize, mLower + rectBcksSize, mUpper + rectBcksSize);

    blockLU->blockForwSubs(mLower + rectBcksSize, RHS + sqrBcksDim);
    blockLU->blockForwSubs(
            mLower + rectBcksSize, extraLeftRHS + sqrBcksSize, sqrBcksDim);
    blockLU->blockForwSubs(
            mLower + rectBcksSize, extraRightRHS + sqrBcksSize, sqrBcksDim);

    blockLU->blockBackSubs(
            mDiag + sqrBcksSize, mUpper + rectBcksSize, RHS + sqrBcksDim);
    blockLU->blockBackSubs(mDiag + sqrBcksSize, mUpper + rectBcksSize,
            extraLeftRHS + sqrBcksSize, sqrBcksDim);
    blockLU->blockBackSubs(mDiag + sqrBcksSize, mUpper + rectBcksSize,
            extraRightRHS + sqrBcksSize, sqrBcksDim);
#ifdef PRINT_BIG_SYSTEM_MATLAB_NUSBTS
    std::cerr << "sol = zeros(" << (nSqrBcks - 2) * sqrBcksDim << ","
              << 1 + 2 * sqrBcksDim << ");\n";
    std::cerr << "sol(:,:) = [";
    for (std::size_t i = 0; i != (nSqrBcks - 2) * sqrBcksDim; i++) {
        std::cerr << RHS[i + sqrBcksDim] << " ";
        for (std::size_t j = 0; j < sqrBcksDim; ++j)
            std::cerr << extraLeftRHS[sqrBcksSize + i * sqrBcksDim + j] << " ";
        for (std::size_t j = 0; j < sqrBcksDim; ++j)
            std::cerr << extraRightRHS[sqrBcksSize + i * sqrBcksDim + j] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];\n";
    exit(1);
#endif
    if (returned_value != 1)
        return 0;
    else
        return 1;
}

int
NUSBTS::solve()
{
//#define PRINT_WHOLE_SYSTEM_MATLAB_NUSBTS
#ifdef PRINT_WHOLE_SYSTEM_MATLAB_NUSBTS
    std::cerr << std::scientific << std::setprecision(16);
    std::cerr << "dtriplets = zeros(" << sqrArraySize << ",3);\n";
    std::cerr << "utriplets = zeros(" << sqrArraySize << ",3);\n";
    std::cerr << "ltriplets = zeros(" << sqrArraySize << ",3);\n";
    std::cerr << "rhs = zeros(" << RHSArraySize << ",1);\n";
    std::cerr << "\n% block diagonal entries\n";
    for (std::size_t i = 0, ii = 1; i != nSqrBcks; ++i)
        for (std::size_t j = 0; j != sqrBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "dtriplets(" << ii << ", :) = ["
                          << 1 + i * sqrBcksDim + j << ", "
                          << 1 + i * sqrBcksDim + k << ", "
                          << mDiag[i * sqrBcksSize + j * sqrBcksDim + k]
                          << "];\n";
    std::cerr << "\n% block lower entries\n";
    for (std::size_t i = 0, ii = 1; i != nRectBcks; ++i)
        for (std::size_t j = 0; j != rectBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "ltriplets(" << ii << ", :) = ["
                          << 1 + (i + 1) * sqrBcksDim + zeroBcksDim + j << ", "
                          << 1 + i * sqrBcksDim + k << ", "
                          << mLower[i * rectBcksSize + j * sqrBcksDim + k]
                          << "];\n";
    for (std::size_t j = 0, ii = rectArraySize + 1; j != rectBcksDim; ++j)
        for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
            std::cerr << "ltriplets(" << ii << ", :) = ["
                      << 1 + nRectBcks * sqrBcksDim + zeroBcksDim + j << ", "
                      << 1 + (nRectBcks - 2) * sqrBcksDim + k << ", "
                      << rightBdryExtraBck[j * sqrBcksDim + k] << "];\n";
    std::cerr << "\n% block upper entries\n";
    for (std::size_t i = 0, ii = 1; i != nRectBcks; ++i)
        for (std::size_t j = 0; j != rectBcksDim; ++j)
            for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
                std::cerr << "utriplets(" << ii << ", :) = ["
                          << 1 + i * sqrBcksDim + zeroBcksDim + j << ", "
                          << 1 + (i + 1) * sqrBcksDim + k << ", "
                          << mUpper[i * rectBcksSize + j * sqrBcksDim + k]
                          << "];\n";
    for (std::size_t j = 0, ii = rectArraySize + 1; j != rectBcksDim; ++j)
        for (std::size_t k = 0; k != sqrBcksDim; ++k, ++ii)
            std::cerr << "utriplets(" << ii << ", :) = [" << 1 + zeroBcksDim + j
                      << ", " << 1 + 2 * sqrBcksDim + k << ", "
                      << leftBdryExtraBck[j * sqrBcksDim + k] << "];\n";
    std::cerr << "\n% rhs entries\n";
    std::cerr << "rhs(:,1) = [";
    for (std::size_t i = 0; i != RHSArraySize; i++)
        std::cerr << RHS[i] << std::endl;
    std::cerr << "];\n";
    std::cerr << "\n% bulding the sparse matrix\n";
    std::cerr << "alltriplets = [dtriplets ; utriplets ; ltriplets];\n";
    std::cerr << "S = "
                 "sparse(alltriplets(:,1),alltriplets(:,2),alltriplets(:,3));"
                 "\n\n\n";
#endif

    /*
     * Fill the two RHS with the proper values
     */
    FillextraRHS();

    /*
     * solves the block tridiagonal systems
     */
    int eliminationResult = (this->*solveLinearSystem)();

    /*
     * solves the small system
     */
    if (eliminationResult > 0) {
        CalculatesSmallSystem(mDiag, mUpper, extraLeftRHS + sqrBcksSize,
                extraRightRHS + sqrBcksSize, mDiag + sqrArraySize - sqrBcksSize,
                mLower + rectArraySize - rectBcksSize,
                extraLeftRHS + sqrArraySize - 3 * sqrBcksSize,
                extraRightRHS + sqrArraySize - 3 * sqrBcksSize, RHS,
                RHS + RHSArraySize - sqrBcksDim, RHS + sqrBcksDim,
                RHS + RHSArraySize - 3 * sqrBcksDim);
        eliminationResult
                = solveSmallSystem(mDiag, mDiag + sqrArraySize - sqrBcksSize,
                        mUpper, mLower + rectArraySize - rectBcksSize, RHS,
                        RHS + RHSArraySize - sqrBcksDim);

        if (eliminationResult > 0) {
            /*
             * Adds -\overline{ \gamma } x_1 to \overline{ b^' }
             */
            NUBCS::AddMatrixProduct(extraLeftRHS + sqrBcksSize, RHS,
                    RHS + sqrBcksDim, -1.0, RHSArraySize - 2 * sqrBcksDim,
                    sqrBcksDim, 1);

            /*
             * Adds -\overline{ \phi } x_N to the previous \overline{ b^' }
             */
            NUBCS::AddMatrixProduct(extraRightRHS + sqrBcksSize,
                    RHS + RHSArraySize - sqrBcksDim, RHS + sqrBcksDim, -1.0,
                    RHSArraySize - 2 * sqrBcksDim, sqrBcksDim, 1);
        }
    }

#ifdef PRINT_WHOLE_SYSTEM_MATLAB_NUSBTS
    std::cerr << "sol = zeros(" << RHSArraySize << ",1);\n";
    std::cerr << "\n% sol entries\n";
    std::cerr << "sol(:,1) = [";
    for (std::size_t i = 0; i != RHSArraySize; i++)
        std::cerr << RHS[i] << std::endl;
    std::cerr << "];\n";
    exit(1);
#endif

    return eliminationResult;
}

} // namespace blots