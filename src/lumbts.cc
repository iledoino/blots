#include "lumbts.h"
#include "util.h"
#include <stdexcept>

namespace blots
{

/*
 * Constructor
 */
LUMBTS::LUMBTS(std::size_t nOfBlocks_, std::size_t blockDim_,
        const double& upperCondLimit_)
    : fullLU(blockDim_)
    , upperCond(1.)
    , upperCondLimit(upperCondLimit_)
    , nOfBlocks(nOfBlocks_)
    , blockDim(blockDim_)
    , blockSize(blockDim_ * blockDim_)
    , ipvi(blockDim * nOfBlocks)
{
}

int
LUMBTS::blockDecomp(double* D, double* L, double* U)
{
    int returned_value = 1;
    double upperCondAux = 0.;
    upperCond = 1.0;

    for (std::size_t i = 0, iN = blockSize, j = 0;
            i < blockSize * (nOfBlocks - 1);
            i = iN, iN += blockSize, j += blockDim) {
        // trecho que encontra o bloco \overline{B}_i = B_i
        // (\overline{D}_i)^{-1}
        if (fullLU.decomp(D + i, ipvi.data() + j, 1.0e-16, upperCondAux) != 1)
            returned_value = 0;
        upperCond *= upperCondAux * 0.5;
        fullLU.forwBackSubsT(D + i, L + i, blockDim, ipvi.data() + j);

        // encontra \overline{D}_{i+1}, dado por \overline{D}_i =
        // D_i-\overline{B}_{i}*\overline{P}_{i}
        block_dot(L + i, U + i, D + iN, blockDim, blockDim, blockDim, -1.0);
    }
    if (fullLU.decomp(D + blockSize * (nOfBlocks - 1),
                ipvi.data() + blockDim * (nOfBlocks - 1), 1.0e-16, upperCondAux)
            != 1)
        returned_value = 0;
    upperCond *= upperCondAux;

    if (returned_value == 0) {
        std::cout << "LUMBTS::blockDecomp: Error: found a singular "
                     "diagonal block."
                  << std::endl;
    }
    else if (upperCond > upperCondLimit) {
        std::cout << "LUMBTS::blockDecomp: Error: the condition "
                     "number of the block tridiagonal matrix is greater than "
                  << upperCondLimit << "." << std::endl;
        returned_value = -1;
    }

    return returned_value;
}

int
LUMBTS::blockDecomp(
        std::vector<double>& D, std::vector<double>& L, std::vector<double>& U)
{
    return blockDecomp(D.data(), L.data(), U.data());
}

void
LUMBTS::blockForwSubs(double* L, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSBcksSize(blockDim * nOfRHS);
    for (std::size_t i = 0, iN = RHSBcksSize, j = 0;
            i < RHSBcksSize * (nOfBlocks - 1);
            i = iN, iN += RHSBcksSize, j += blockSize) {
        // b_{i+1} = b_{i+1} - B_i*b_i
        block_dot(L + j, RHS + i, RHS + iN, blockDim, blockDim, nOfRHS, -1.0);
    }
}

void
LUMBTS::blockForwSubs(
        std::vector<double>& L, std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockForwSubs(L.data(), RHS.data(), nOfRHS);
}

void
LUMBTS::blockForwSubsT(double* D, double* U, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSLD(blockDim * (nOfBlocks - 1));

    // Y_1 = Y_1 * (D_1)^{-1}
    fullLU.forwBackSubsT(D, RHS, nOfRHS, ipvi.data(), RHSLD);

    for (std::size_t ii = blockDim, i = 0, iN = blockDim, j = 0, jN = blockSize;
            ii <= RHSLD;
            ii += blockDim, i = iN, iN += blockDim, j = jN, jN += blockSize) {
        // Y_{i+1} = Y_{i+1} - Y_i * B_i
        block_dot(RHS + i, U + j, RHS + iN, nOfRHS, blockDim, blockDim, -1.0,
                RHSLD, 0, RHSLD);

        // Y_{i+1} = Y_{i+1} * D_{i+1}^{-1}
        fullLU.forwBackSubsT(D + jN, RHS + iN, nOfRHS, ipvi.data() + ii, RHSLD);
    }
}

void
LUMBTS::blockForwSubsT(std::vector<double>& D, std::vector<double>& U,
        std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockForwSubsT(D.data(), U.data(), RHS.data(), nOfRHS);
}

void
LUMBTS::blockBackSubs(double* D, double* U, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSBcksSize(blockDim * nOfRHS);

    // Y_M = (D_M)^{-1}*Y_M
    fullLU.forwBackSubs(D + blockSize * (nOfBlocks - 1),
            RHS + RHSBcksSize * (nOfBlocks - 1), nOfRHS,
            ipvi.data() + blockDim * (nOfBlocks - 1));

    for (std::size_t ii = (nOfBlocks - 1), i = blockDim * (nOfBlocks - 2),
                     iB = blockSize * (nOfBlocks - 2),
                     iRHS = RHSBcksSize * (nOfBlocks - 2),
                     iRHSN = iRHS + RHSBcksSize;
            ii > 0; --ii, i -= blockDim, iB -= blockSize, iRHSN = iRHS,
                     iRHS -= RHSBcksSize) {
        // Y_{i-1} = Y_{i-1} - S_i*Y_i
        block_dot(U + iB, RHS + iRHSN, RHS + iRHS, blockDim, blockDim, nOfRHS,
                -1.0);

        // Y_{i-1} = (D_{i-1})^{-1}*Y_{i-1}
        fullLU.forwBackSubs(D + iB, RHS + iRHS, nOfRHS, ipvi.data() + i);
    }
}

void
LUMBTS::blockBackSubs(std::vector<double>& D, std::vector<double>& U,
        std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockBackSubs(D.data(), U.data(), RHS.data(), nOfRHS);
}

void
LUMBTS::blockBackSubsT(double* L, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSLD(blockDim * (nOfBlocks - 1));

    for (std::size_t i = blockDim * (nOfBlocks - 2), iN = i + blockDim,
                     j = blockSize * (nOfBlocks - 2);
            iN > 0; iN = i, i -= blockDim, j -= blockSize) {
        // b_{i+1} = b_{i+1} - b_i*B_i
        block_dot(RHS + iN, L + j, RHS + i, nOfRHS, blockDim, blockDim, -1.0,
                RHSLD, 0, RHSLD);
    }
}

void LUMBTS::blockBackSubsT(std::vector<double>& L, std::vector<double>& RHS,
    std::size_t nOfRHS)
{
    blockBackSubsT(L.data(), RHS.data(), nOfRHS);
}

int
LUMBTS::solve(double* D, double* L, double* U, double* RHS, std::size_t nOfRHS)
{
    int i = blockDecomp(D, L, U);
    blockForwSubs(L, RHS, nOfRHS);
    blockBackSubs(D, U, RHS, nOfRHS);
    return i;
}

int
LUMBTS::solve(std::vector<double>& D, std::vector<double>& L,
        std::vector<double>& U, std::vector<double>& RHS, std::size_t nOfRHS)
{
    return solve(D.data(), L.data(), U.data(), RHS.data(), nOfRHS);
}

int
LUMBTS::solveT(double* D, double* L, double* U, double* RHS, std::size_t nOfRHS)
{
    int i = blockDecomp(D, L, U);
    blockForwSubsT(D, U, RHS, nOfRHS);
    blockBackSubsT(L, RHS, nOfRHS);
    return i;
}

int
LUMBTS::solveT(std::vector<double>& D, std::vector<double>& L,
        std::vector<double>& U, std::vector<double>& RHS, std::size_t nOfRHS)
{
    return solveT(D.data(), L.data(), U.data(), RHS.data(), nOfRHS);
}

} // namespace blots