#include "cdmbts.h"

namespace blots
{

std::size_t
CDMBTS::setUpPartition(std::size_t nPartitions_, const std::size_t nOfBlocks_)
{
    if (nOfBlocks_ >= (2 * nPartitions_ - 1))
        return nPartitions_;

    while ((nOfBlocks_ < (2 * nPartitions_ - 1)) && nPartitions_ > 0)
        --nPartitions_;

    return nPartitions_;
}

std::size_t
CDMBTS::setUpThreads(
        const std::size_t nThreads_, const std::size_t nPartitions_)
{
    std::size_t nThreads_p = nThreads_;

    if (nThreads_ > nPartitions_)
        nThreads_p = nPartitions_;

    threadStart = new std::size_t[nThreads_p];
    threadEnd = new std::size_t[nThreads_p];

    std::size_t rest = nPartitions_ % nThreads_p;
    std::size_t idealNumberOfIndexes = (nPartitions_ - rest) / nThreads_p;

    threadStart[0] = 0;
    for (std::size_t i = 1; i != (rest + 1); ++i)
        threadStart[i] = threadStart[i - 1] + idealNumberOfIndexes + 1;
    for (std::size_t i = (rest + 1); i != nThreads_p; ++i)
        threadStart[i] = threadStart[i - 1] + idealNumberOfIndexes;

    threadEnd[nThreads_p - 1] = (nPartitions_ - 1);
    for (std::size_t i = (nThreads_p - 1); i > 0; --i)
        threadEnd[i - 1] = threadStart[i] - 1;

    return nThreads_p;
}

void
CDMBTS::Allocate()
{
    D_p = new double[blockSize * (nPartitions - 1)];
    U_p = new double[blockSize * (nPartitions - 1)];
    L_p = new double[blockSize * (nPartitions - 1)];
    RHS_p = new double[blockDim * (nPartitions - 1)];

    Ai_D = new double*[nPartitions];
    Ai_L = new double*[nPartitions];
    Ai_U = new double*[nPartitions];
    RHSi = new double*[nPartitions];

    sMatrix = new std::size_t[nPartitions];
    sVector = new std::size_t[nPartitions];
    iMatrix = new std::size_t[nPartitions];
    iVector = new std::size_t[nPartitions];

    Theta = new double*[nPartitions - 1];
    Omega = new double*[nPartitions - 1];

    /*
     * auxiliar variables to set up sMatrix and sVector
     */
    std::size_t availableBlocksToDistributeAmongPartitions
            = nOfBlocks - (nPartitions - 1);
    std::size_t rest = availableBlocksToDistributeAmongPartitions % nPartitions;
    std::size_t idealNOfBlocksOnEachPartition
            = (availableBlocksToDistributeAmongPartitions - rest) / nPartitions;

    /*
     * lines of code that set up the size of the arrays for each partition
     */
    for (std::size_t i = 0; i < nPartitions; ++i) {
        sMatrix[i] = blockSize * idealNOfBlocksOnEachPartition;
        sVector[i] = blockDim * idealNOfBlocksOnEachPartition;
    }
    for (std::size_t i = 0; i < rest; ++i) {
        sMatrix[i] = sMatrix[i] + blockSize;
        sVector[i] = sVector[i] + blockDim;
    }

    /*
     * once the sizes are defined, one can now define the indices
     */
    iMatrix[0] = 0;
    iVector[0] = 0;
    for (std::size_t i = 1; i < nPartitions; ++i) {
        iMatrix[i] = iMatrix[i - 1] + sMatrix[i - 1] + blockSize;
        iVector[i] = iVector[i - 1] + sVector[i - 1] + blockDim;
    }

    /*
     * These array of pointers are used to more easily execute the method
     */
    for (std::size_t i = 0; i < nPartitions; ++i) {
        Ai_D[i] = D + iMatrix[i];
        Ai_L[i] = L + iMatrix[i];
        Ai_U[i] = U + iMatrix[i];
        RHSi[i] = RHS + iVector[i];
    }

    /*
     * These arrays are the extra memory used on the method
     */
    for (std::size_t i = 0; i < nPartitions - 1; ++i) {
        Theta[i] = new double[sMatrix[i]];
        Omega[i] = new double[sMatrix[i + 1]];
    }
}

CDMBTS::CDMBTS(double* D_, double* L_, double* U_, double* RHS_,
        const std::size_t nOfBlocks_, const std::size_t blockDim_,
        const std::size_t nPartitions_, const std::size_t nThreads_)
    : D(D_)
    , L(L_)
    , U(U_)
    , RHS(RHS_)
    , nOfBlocks(nOfBlocks_)
    , blockDim(blockDim_)
    , blockSize(blockDim_ * blockDim_)
    , threadStart(nullptr)
    , threadEnd(nullptr)
    , nPartitions(setUpPartition(nPartitions_, nOfBlocks_))
    , nThreads(
              setUpThreads(nThreads_, setUpPartition(nPartitions_, nOfBlocks_)))
    , allocatedInside(false)
    , sMatrix(nullptr)
    , sVector(nullptr)
    , iMatrix(nullptr)
    , iVector(nullptr)
    , D_p(nullptr)
    , L_p(nullptr)
    , U_p(nullptr)
    , RHS_p(nullptr)
    , Theta(nullptr)
    , Omega(nullptr)
    , Ai_D(nullptr)
    , Ai_L(nullptr)
    , Ai_U(nullptr)
    , RHSi(nullptr)
    , upperCondLimit_(1.0e5)
    , smallSystem(nPartitions_ - 1, blockDim_, upperCondLimit_)
{
    Allocate();
}

CDMBTS::CDMBTS(const std::size_t nOfBlocks_, const std::size_t blockDim_,
        const std::size_t nPartitions_, const std::size_t nThreads_)
    : D(nullptr)
    , L(nullptr)
    , U(nullptr)
    , RHS(nullptr)
    , nOfBlocks(nOfBlocks_)
    , blockDim(blockDim_)
    , blockSize(blockDim_ * blockDim_)
    , threadStart(nullptr)
    , threadEnd(nullptr)
    , nPartitions(setUpPartition(nPartitions_, nOfBlocks_))
    , nThreads(
              setUpThreads(nThreads_, setUpPartition(nPartitions_, nOfBlocks_)))
    , allocatedInside(true)
    , sMatrix(nullptr)
    , sVector(nullptr)
    , iMatrix(nullptr)
    , iVector(nullptr)
    , D_p(nullptr)
    , L_p(nullptr)
    , U_p(nullptr)
    , RHS_p(nullptr)
    , Theta(nullptr)
    , Omega(nullptr)
    , Ai_D(nullptr)
    , Ai_L(nullptr)
    , Ai_U(nullptr)
    , RHSi(nullptr)
    , upperCondLimit_(1.0e5)
    , smallSystem(nPartitions_ - 1, blockDim_, upperCondLimit_)
{
    D = new double[blockSize * nOfBlocks];
    L = new double[blockSize * (nOfBlocks - 1)];
    U = new double[blockSize * (nOfBlocks - 1)];
    RHS = new double[blockDim * nOfBlocks];

    Allocate();
}

CDMBTS::~CDMBTS()
{
    if (allocatedInside) {
        delete[] D;
        delete[] U;
        delete[] L;
        delete[] RHS;
    }
    delete[] D_p;
    delete[] U_p;
    delete[] L_p;
    delete[] RHS_p;
    delete[] sMatrix;
    delete[] sVector;
    delete[] iMatrix;
    delete[] iVector;
    delete[] Ai_D;
    delete[] Ai_L;
    delete[] Ai_U;
    delete[] RHSi;

    for (std::size_t i = 0; i < nPartitions - 1; ++i) {
        delete[] Theta[i];
        delete[] Omega[i];
    }

    delete[] Theta;
    delete[] Omega;

    delete[] threadEnd;
    delete[] threadStart;
}

void
CDMBTS::SetThetaAndOmega()
{
#ifdef _OPENMP
#pragma omp parallel default(shared) num_threads(nThreads)
    {
        std::size_t i = omp_get_thread_num();
#else
    for (std::size_t i = 0; i < nThreads; ++i) {
#endif /* _OPENMP */
        std::size_t start = threadStart[i];
        std::size_t end = (i + 1) != nThreads ? threadEnd[i] : threadEnd[i] - 1;

        for (i = start; i <= end; ++i) {
            for (std::size_t j = 0; j < (sMatrix[i] - blockSize); j++)
                Theta[i][j] = 0.0;
            for (std::size_t j = (sMatrix[i] - blockSize); j < sMatrix[i]; j++)
                Theta[i][j] = Ai_U[i][j];

            for (std::size_t j = sMatrix[i + 1] - 1; j >= blockSize; j--)
                Omega[i][j] = 0.0;
            for (std::size_t j = 0; j < blockSize; j++)
                Omega[i][j] = Ai_L[i + 1][j - blockSize];
        }
    }
}

void
CDMBTS::PartialDecompositionAndSolving(
        double* A, double* Bck1, double* Bck2, double* RHSv)
{
    std::size_t r, t;
    double tmp;

    /*
     * This loop goes through indices from principal diagonal A
     * It also goes through rows of Bck1 and Bck2.
     * After this loop has ended, the direct product LY=B is done or
     * matrices B=Bck1,Bck2 and RHSv.
     */
    for (std::size_t i = 0; i != (blockDim - 1); ++i) {
        /*
         * Following loop verifies which is the greatest absolute value
         * of an entry that is on (i+1)th column and among lines i+1 to
         * blockDim.
         */
        t = i;
        for (std::size_t j = i + 1; j != blockDim; ++j)
            if (fabs(A[t * blockDim + i]) < fabs(A[j * blockDim + i]))
                t = j;

        /*
         * Once the greatest value has been found, then it is necessary
         * to reposition the matrix in order to use this value as a pivot
         */
        r = i * blockDim;
        if (t != i) {
            tmp = RHSv[i];
            RHSv[i] = RHSv[t];
            RHSv[t] = tmp;

            t = t * blockDim;
            for (std::size_t j = r, k = t; j != (r + blockDim); ++j, ++k) {
                tmp = A[j];
                A[j] = A[k];
                A[k] = tmp;

                tmp = Bck1[j];
                Bck1[j] = Bck1[k];
                Bck1[k] = tmp;

                tmp = Bck2[j];
                Bck2[j] = Bck2[k];
                Bck2[k] = tmp;
            }
        }

        /*
         * Assuming that the decomposition of A was made for last
         * iteration, then Bck1 and Bck2 are updated with the values
         * stored on the same matrix A. This is the direct system solving
         * for the linear system LUX=B, where LU is the LU decomposition
         * of A, stored in the same A. B is Bck1 or Bck2, and X is stored
         * in the vary same arrays Bck1 and Bck2.
         */
        for (std::size_t j = 0, jB = 0; j != i; ++j, jB += blockDim) {
            tmp = A[r + j];
            for (t = 0; t != blockDim; ++t) {
                Bck1[r + t] -= tmp * Bck1[jB + t];
                Bck2[r + t] -= tmp * Bck2[jB + t];
            }
            RHSv[i] -= tmp * RHSv[j];
        }

        /*
         * This loop goes throgh every entry that is on the same column
         * of the pivot and is on line above it. Then it uses this entry
         * as a holder for a value that will be used for the direct
         * system solving. After this, an update is done on matrix A.
         * The overall work after this is a partial decomposition of A
         * in LU, stored in A
         */
        for (std::size_t j = (i + 1) * blockDim; j != blockSize;
                j += blockDim) {
            A[j + i] /= A[r + i];
            tmp = A[j + i];
            for (t = i + 1; t != blockDim; ++t)
                A[j + t] -= tmp * A[r + t];
        }
    }
    /*
     * Assuming that the decomposition of A was made for the last
     * iteration, then Bck1 and Bck2 are updated with the values
     * stored on the same matrix A. This is the direct system solving
     * for the linear system LUX=B, where LU is the LU decomposition
     * of A, stored in the same A. B is Bck1 or Bck2, and X is stored
     * in the vary same arrays Bck1 and Bck2.
     */
    r = (blockDim - 1) * blockDim;
    for (std::size_t j = 0, jB = 0; j != (blockDim - 1); ++j, jB += blockDim) {
        tmp = A[r + j];
        for (t = 0; t != blockDim; ++t) {
            Bck1[r + t] -= tmp * Bck1[jB + t];
            Bck2[r + t] -= tmp * Bck2[jB + t];
        }
        RHSv[(blockDim - 1)] -= tmp * RHSv[j];
    }

    /*
     * This is the loop that does the final system solving UX=Y
     */
    for (std::size_t i = blockDim, iB = (blockDim - 1) * blockDim; i > 0;
            i--, iB -= blockDim) {
        for (std::size_t j = blockDim, jB = (blockDim - 1) * blockDim; j > i;
                j--, jB -= blockDim) {
            tmp = A[iB + j - 1];
            for (t = 0; t != blockDim; ++t) {
                Bck1[iB + t] -= tmp * Bck1[jB + t];
                Bck2[iB + t] -= tmp * Bck2[jB + t];
            }
            RHSv[i - 1] -= tmp * RHSv[j - 1];
        }

        tmp = 1.0 / A[iB + i - 1];
        for (t = 0; t < blockDim; t++) {
            Bck1[iB + t] *= tmp;
            Bck2[iB + t] *= tmp;
        }
        RHSv[i - 1] *= tmp;
    }
}

void
CDMBTS::PartialDecompositionAndSolving(double* A, double* Bck1, double* RHSv)
{
    std::size_t r, t;
    double tmp;

    /*
     * This loop goes through indices from principal diagonal A
     * It also goes through rows of Bck1 and Bck2.
     * After this loop has ended, the direct product LY=B is done or
     * matrices B=Bck1,Bck2 and RHSv.
     */
    for (std::size_t i = 0; i != (blockDim - 1); ++i) {
        /*
         * Following loop verifies which is the greatest absolute value
         * of an entry that is on (i+1)th column and among lines i+1 to
         * blockDim.
         */
        t = i;
        for (std::size_t j = i + 1; j != blockDim; ++j)
            if (fabs(A[t * blockDim + i]) < fabs(A[j * blockDim + i]))
                t = j;

        /*
         * Once the greatest value has been found, then it is necessary
         * to reposition the matrix in order to use this value as a pivot
         */
        r = i * blockDim;
        if (t != i) {
            tmp = RHSv[i];
            RHSv[i] = RHSv[t];
            RHSv[t] = tmp;

            t = t * blockDim;
            for (std::size_t j = r, k = t; j != (r + blockDim); ++j, ++k) {
                tmp = A[j];
                A[j] = A[k];
                A[k] = tmp;

                tmp = Bck1[j];
                Bck1[j] = Bck1[k];
                Bck1[k] = tmp;
            }
        }

        /*
         * Assuming that the decomposition of A was made for last
         * iteration, then Bck1 and Bck2 are updated with the values
         * stored on the same matrix A. This is the direct system solving
         * for the linear system LUX=B, where LU is the LU decomposition
         * of A, stored in the same A. B is Bck1 or Bck2, and X is stored
         * in the vary same arrays Bck1 and Bck2.
         */
        for (std::size_t j = 0, jB = 0; j != i; ++j, jB += blockDim) {
            tmp = A[r + j];
            for (t = 0; t != blockDim; ++t)
                Bck1[r + t] -= tmp * Bck1[jB + t];
            RHSv[i] -= tmp * RHSv[j];
        }

        /*
         * This loop goes throgh every entry that is on the same column
         * of the pivot and is on line above it. Then it uses this entry
         * as a holder for a value that will be used for the direct
         * system solving. After this, an update is done on matrix A.
         * The overall work after this is a partial decomposition of A
         * in LU, stored in A
         */
        for (std::size_t j = (i + 1) * blockDim; j != blockSize;
                j += blockDim) {
            A[j + i] /= A[r + i];
            tmp = A[j + i];
            for (t = i + 1; t != blockDim; ++t)
                A[j + t] -= tmp * A[r + t];
        }
    }
    /*
     * Assuming that the decomposition of A was made for the last
     * iteration, then Bck1 and Bck2 are updated with the values
     * stored on the same matrix A. This is the direct system solving
     * for the linear system LUX=B, where LU is the LU decomposition
     * of A, stored in the same A. B is Bck1 or Bck2, and X is stored
     * in the vary same arrays Bck1 and Bck2.
     */
    r = (blockDim - 1) * blockDim;
    for (std::size_t j = 0, jB = 0; j != (blockDim - 1); ++j, jB += blockDim) {
        tmp = A[r + j];
        for (t = 0; t != blockDim; ++t)
            Bck1[r + t] -= tmp * Bck1[jB + t];
        RHSv[(blockDim - 1)] -= tmp * RHSv[j];
    }

    /*
     * This is the loop that does the final system solving UX=Y
     */
    for (std::size_t i = blockDim, iB = (blockDim - 1) * blockDim; i > 0;
            i--, iB -= blockDim) {
        for (std::size_t j = blockDim, jB = (blockDim - 1) * blockDim; j > i;
                j--, jB -= blockDim) {
            tmp = A[iB + j - 1];
            for (t = 0; t != blockDim; ++t)
                Bck1[iB + t] -= tmp * Bck1[jB + t];
            RHSv[i - 1] -= tmp * RHSv[j - 1];
        }

        tmp = 1.0 / A[iB + i - 1];
        for (t = 0; t < blockDim; t++)
            Bck1[iB + t] *= tmp;
        RHSv[i - 1] *= tmp;
    }
}

void
CDMBTS::UpdateNextBlocks(double* UpdatingB, double* Omegab, double* Thetab,
        double* RHSv, double* newOmegab, double* newThetab, double* newRHSv)
{
    double tmp;

    for (std::size_t k = 0, kb = 0; k != blockDim; k++, kb += blockDim)
        for (std::size_t i = 0, ib = 0; i != blockDim; i++, ib += blockDim) {
            tmp = UpdatingB[kb + i];
            for (std::size_t j = 0; j != blockDim; j++) {
                newOmegab[kb + j] -= tmp * Omegab[ib + j];
                newThetab[kb + j] -= tmp * Thetab[ib + j];
            }
            newRHSv[k] -= tmp * RHSv[i];
        }
}

void
CDMBTS::UpdateNextBlocks(double* UpdatingB, double* Block, double* RHSv,
        double* newBlock, double* newRHSv)
{
    double tmp;

    for (std::size_t k = 0, kb = 0; k != blockDim; k++, kb += blockDim)
        for (std::size_t i = 0, ib = 0; i != blockDim; i++, ib += blockDim) {
            tmp = UpdatingB[kb + i];
            for (std::size_t j = 0; j != blockDim; j++)
                newBlock[kb + j] -= tmp * Block[ib + j];
            newRHSv[k] -= tmp * RHSv[i];
        }
}

void
CDMBTS::MatrixMatrixProduct(double* SolutionB, double* M_L, double* M_R)
{
    double tmp;

    for (std::size_t t = 0; t != blockSize; t += blockDim) {
        // i=0;
        tmp = M_L[t];
        for (std::size_t j = 0, k = t; j != blockDim; ++j, ++k)
            SolutionB[k] = -tmp * M_R[j];

        for (std::size_t i = 1, k = blockDim; i != blockDim;
                ++i, k += blockDim) {
            tmp = M_L[t + i];
            for (std::size_t j = 0; j != blockDim; ++j)
                SolutionB[t + j] -= tmp * M_R[k + j];
        }
    }
}

void
CDMBTS::AddBlockAndMatrixMatrixProduct(
        double* SolutionB, double* AddingB, double* M_L, double* M_R)
{
    double tmp;

    for (std::size_t t = 0; t != blockSize; t += blockDim) {
        // i=0;
        tmp = M_L[t];
        for (std::size_t j = 0, k = t; j != blockDim; ++j, ++k)
            SolutionB[k] += AddingB[k] - tmp * M_R[j];

        for (std::size_t i = 1, k = blockDim; i != blockDim;
                ++i, k += blockDim) {
            tmp = M_L[t + i];
            for (std::size_t j = 0; j != blockDim; ++j)
                SolutionB[t + j] -= tmp * M_R[k + j];
        }
    }
}

void
CDMBTS::MatrixVectorProduct(double* SolutionV, double* M_L, double* V_R)
{
    for (std::size_t i = 0, k = 0; i < blockDim; i++, k += blockDim) {
        // j=0;
        SolutionV[i] = -M_L[k] * V_R[0];
        for (std::size_t j = 1; j < blockDim; j++)
            SolutionV[i] -= M_L[k + j] * V_R[j];
    }
}

void
CDMBTS::AddBlockAndMatrixVectorProduct(
        double* SolutionV, double* AddingV, double* M_L, double* V_R)
{
    for (std::size_t i = 0, k = 0; i < blockDim; i++, k += blockDim) {
        // j=0;
        SolutionV[i] += AddingV[i] - M_L[k] * V_R[0];
        for (std::size_t j = 1; j < blockDim; j++)
            SolutionV[i] -= M_L[k + j] * V_R[j];
    }
}

void
CDMBTS::CalculateFinalSolution(double* FS, double* Omegab, double* Thetab,
        double* betav, std::size_t sv)
{
    for (std::size_t i = 0; i != sv; ++i)
        for (std::size_t jC = 0, jN = blockDim, k = i * blockDim;
                jC != blockDim; ++jC, ++jN, ++k)
            FS[i] -= Omegab[k] * betav[jC] + Thetab[k] * betav[jN];
}

void
CDMBTS::CalculateFinalSolution(
        double* FS, double* OmegaOrThetab, double* betav, std::size_t sv)
{
    for (std::size_t i = 0; i != sv; ++i)
        for (std::size_t j = 0, k = i * blockDim; j != blockDim; ++j, ++k)
            FS[i] -= OmegaOrThetab[k] * betav[j];
}

void
CDMBTS::CopyVector(double* VectorCopy, double* VectorCopied, std::size_t n)
{
    std::size_t i;

    for (i = 0; i < n; i++)
        VectorCopy[i] = VectorCopied[i];
}

int
CDMBTS::solve()
{
    SetThetaAndOmega();

#ifdef _OPENMP
#pragma omp parallel default(shared) num_threads(nThreads)
    {
        std::size_t i = omp_get_thread_num();
#else
    for (std::size_t i = 0; i < nThreads; ++i) {
#endif /* _OPENMP */
        std::size_t start = threadStart[i];
        std::size_t end = threadEnd[i];
        std::size_t jv, jvN, jb, jbN;

        for (i = start; i <= end; ++i) {
            if (i == 0) {
                /// If the current partition is the first, then there is no
                // Omega vector ///
                /*
                 * =============================================================
                 *                    DIRECT SUBSTITUTIONS
                 * =============================================================
                 */
                for (jv = 0, jvN = blockDim, jb = 0, jbN = blockSize;
                        jv != sVector[i] - blockDim;
                        jv = jvN, jvN += blockDim, jb = jbN, jbN += blockSize) {
                    PartialDecompositionAndSolving(
                            Ai_D[i] + jb, Ai_U[i] + jb, RHSi[i] + jv);
                    UpdateNextBlocks(Ai_L[i] + jb, Ai_U[i] + jb, RHSi[i] + jv,
                            Ai_D[i] + jbN, RHSi[i] + jvN);
                }
                jv = sVector[i] - blockDim;
                jb = blockDim * jv;
                PartialDecompositionAndSolving(
                        Ai_D[i] + jb, Theta[i] + jb, RHSi[i] + jv);

                /*
                 * =============================================================
                 *                   INVERSE SUBSTITUTIONS
                 * =============================================================
                 */
                for (jv = (sVector[i] - 2 * blockDim),
                    jvN = (sVector[i] - blockDim),
                    jb = (sMatrix[i] - 2 * blockSize),
                    jbN = (sMatrix[i] - blockSize);
                        jvN > 0;
                        jvN = jv, jv -= blockDim, jbN = jb, jb -= blockSize)
                    UpdateNextBlocks(Ai_U[i] + jb, Theta[i] + jbN,
                            RHSi[i] + jvN, Theta[i] + jb, RHSi[i] + jv);
            }
            else if (i == (nPartitions - 1)) {
                /// If the current partition is the last, then there is no Theta
                // vector ///
                /*
                 * =============================================================
                 *                    DIRECT SUBSTITUTIONS
                 * =============================================================
                 */
                for (jv = 0, jvN = blockDim, jb = 0, jbN = blockSize;
                        jv != sVector[i] - blockDim;
                        jv = jvN, jvN += blockDim, jb = jbN, jbN += blockSize) {
                    PartialDecompositionAndSolving(Ai_D[i] + jb,
                            Omega[i - 1] + jb, Ai_U[i] + jb, RHSi[i] + jv);
                    UpdateNextBlocks(Ai_L[i] + jb, Omega[i - 1] + jb,
                            Ai_U[i] + jb, RHSi[i] + jv, Omega[i - 1] + jbN,
                            Ai_D[i] + jbN, RHSi[i] + jvN);
                }
                jv = sVector[i] - blockDim;
                jb = blockDim * jv;
                PartialDecompositionAndSolving(
                        Ai_D[i] + jb, Omega[i - 1] + jb, RHSi[i] + jv);

                /*
                 * =============================================================
                 *                   INVERSE SUBSTITUTIONS
                 * =============================================================
                 */
                for (jv = (sVector[i] - 2 * blockDim),
                    jvN = (sVector[i] - blockDim),
                    jb = (sMatrix[i] - 2 * blockSize),
                    jbN = (sMatrix[i] - blockSize);
                        jvN > 0;
                        jvN = jv, jv -= blockDim, jbN = jb, jb -= blockSize)
                    UpdateNextBlocks(Ai_U[i] + jb, Omega[i - 1] + jbN,
                            RHSi[i] + jvN, Omega[i - 1] + jb, RHSi[i] + jv);

                /*
                 * =============================================================
                 *                 BLOCKS FOR CENTRAL SYSTEM
                 * =============================================================
                 */
                jv = blockDim * (i - 1);
                jb = blockSize * (i - 1);

                MatrixMatrixProduct(
                        D_p + jb, Ai_U[i] - blockSize, Omega[i - 1]);
                MatrixVectorProduct(RHS_p + jv, Ai_U[i] - blockSize, RHSi[i]);
            }
            else {
                /// If the partition is not the first or last, then both are
                // present ///
                /*
                 * =============================================================
                 *                    DIRECT SUBSTITUTIONS
                 * =============================================================
                 */
                for (jv = 0, jvN = blockDim, jb = 0, jbN = blockSize;
                        jv != sVector[i] - blockDim;
                        jv = jvN, jvN += blockDim, jb = jbN, jbN += blockSize) {
                    PartialDecompositionAndSolving(Ai_D[i] + jb,
                            Omega[i - 1] + jb, Ai_U[i] + jb, RHSi[i] + jv);
                    UpdateNextBlocks(Ai_L[i] + jb, Omega[i - 1] + jb,
                            Ai_U[i] + jb, RHSi[i] + jv, Omega[i - 1] + jbN,
                            Ai_D[i] + jbN, RHSi[i] + jvN);
                }
                jv = sVector[i] - blockDim;
                jb = blockDim * jv;
                PartialDecompositionAndSolving(Ai_D[i] + jb, Omega[i - 1] + jb,
                        Theta[i] + jb, RHSi[i] + jv);

                /*
                 * =============================================================
                 *                   INVERSE SUBSTITUTIONS
                 * =============================================================
                 */
                for (jv = (sVector[i] - 2 * blockDim),
                    jvN = (sVector[i] - blockDim),
                    jb = (sMatrix[i] - 2 * blockSize),
                    jbN = (sMatrix[i] - blockSize);
                        jvN > 0;
                        jvN = jv, jv -= blockDim, jbN = jb, jb -= blockSize)
                    UpdateNextBlocks(Ai_U[i] + jb, Omega[i - 1] + jbN,
                            Theta[i] + jbN, RHSi[i] + jvN, Omega[i - 1] + jb,
                            Theta[i] + jb, RHSi[i] + jv);

                /*
                 * =============================================================
                 *                 BLOCKS FOR CENTRAL SYSTEM
                 * =============================================================
                 */
                jv = blockDim * (i - 1);
                jb = blockSize * (i - 1);
                jbN = sMatrix[i] - blockSize;

                MatrixMatrixProduct(U_p + jb, Ai_U[i] - blockSize, Theta[i]);
                MatrixMatrixProduct(
                        L_p + jb, Ai_L[i] + jbN, Omega[i - 1] + jbN);
                MatrixMatrixProduct(
                        D_p + jb, Ai_U[i] - blockSize, Omega[i - 1]);
                MatrixVectorProduct(RHS_p + jv, Ai_U[i] - blockSize, RHSi[i]);
            }
        }
    }

    std::size_t jv, jb;

    for (std::size_t i = 0; i != (nPartitions - 1); ++i) {
        jv = blockDim * i;
        jb = blockSize * i;

        AddBlockAndMatrixMatrixProduct(D_p + jb, Ai_D[i + 1] - blockSize,
                Ai_L[i + 1] - 2 * blockSize, Theta[i] + sMatrix[i] - blockSize);
        AddBlockAndMatrixVectorProduct(RHS_p + jv, RHSi[i + 1] - blockDim,
                Ai_L[i + 1] - 2 * blockSize, RHSi[i] + sVector[i] - blockDim);
    }

    smallSystem.solve(D_p, L_p, U_p, RHS_p);

#ifdef _OPENMP
#pragma omp parallel default(shared) num_threads(nThreads)
    {
        std::size_t i = omp_get_thread_num();
#else
    for (std::size_t i = 0; i < nThreads; ++i) {
#endif /* _OPENMP */
        std::size_t start = threadStart[i];
        std::size_t end = threadEnd[i];

        for (i = start; i <= end; ++i) {
            if (i == 0) {
                CalculateFinalSolution(RHSi[0], Theta[0], RHS_p, sVector[0]);
                CopyVector(RHSi[1] - blockDim, RHS_p, blockDim);
            }
            else if (i == (nPartitions - 1))
                CalculateFinalSolution(RHSi[nPartitions - 1],
                        Omega[nPartitions - 2],
                        RHS_p + blockDim * (nPartitions - 2),
                        sVector[nPartitions - 1]);
            else {
                CalculateFinalSolution(RHSi[i], Omega[i - 1], Theta[i],
                        RHS_p + blockDim * (i - 1), sVector[i]);
                CopyVector(
                        RHSi[i + 1] - blockDim, RHS_p + blockDim * i, blockDim);
            }
        }
    }

    return 1;
}

} // namespace blots