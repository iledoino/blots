#ifndef SRC_CDMBTS_H
#define SRC_CDMBTS_H

/*
 * This software was created by Ismael de Souza Ledoino
 * <ismael.sledoino@gmail.com>, with supervision of Duilio Tadeu
 * <dtadeu@gmail.com> (Universidade Federal Rural do Rio de Janeiro
 * - UFRRJ) and Dan Marchesin <marchesi@impa.br> (Instituto de
 * Matematica Pura e Aplicada - IMPA). Commentaries and suggestions,
 * or even questions, please email the developer.
 */
#include "lumbts.h"
#include "lumnubts.h"
#include <cmath>
#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#endif /* _OPENMP */

namespace blots
{

/*
 * Conquer and Divide Method for solving uniform block tridiagonal linear
 * systems in parallel (shared memory machines)
 */
class CDMBTS
{
public:
    CDMBTS(const std::size_t nOfBlocks_, const std::size_t blockDim_,
            const std::size_t nPartitions_, const std::size_t nThreads_);
    CDMBTS(double* D_, double* L_, double* U_, double* RHS_,
            const std::size_t nOfBlocks_, const std::size_t blockDim_,
            const std::size_t nPartitions_, const std::size_t nThreads_);
    virtual ~CDMBTS();

    /* Pointers to hold the three arrays of blocks and the right hand side */
    double *D, *L, *U, *RHS;
    /* variables to specify the number of blocks and the block's dimension */
    const std::size_t nOfBlocks, blockDim, blockSize;
    std::size_t *threadStart, *threadEnd;
    const std::size_t nPartitions, nThreads;

    /* Function that solves the linear system, returning 1 in case
     * it was succesfully solved */
    int solve();

    double upperCondLimit() const { return upperCondLimit_; }
    double& upperCondLimit() { return upperCondLimit_; }

private:
    CDMBTS(const CDMBTS&) = delete;
    CDMBTS& operator=(const CDMBTS&) = delete;

    /* This constant specifies whether the arrays D, L, and U are
     * allocated inside the constructor or not */
    const bool allocatedInside;
    /* Arrays used to deal with the several partitions */
    std::size_t *sMatrix, *sVector, *iMatrix, *iVector;
    /* Pointers used to solve the central Linear System */
    double *D_p, *L_p, *U_p, *RHS_p;
    /* Pointers used to organize the Conquer and Divide Method */
    double **Theta, **Omega;
    double **Ai_D, **Ai_L, **Ai_U, **RHSi;
    /* Class that solves the block tridiagonal system originated from
     * conquer and division decomposition */
    double upperCondLimit_;
    LUMBTS smallSystem;

    /* Function called inside the constructors */
    void Allocate();
    /* Function that sets up the extra memory used on this method */
    void SetThetaAndOmega();
    /* Function that partially does the decomposition of the method */
    void PartialDecompositionAndSolving(
            double* A, double* Bck1, double* Bck2, double* RHS);
    void PartialDecompositionAndSolving(double* A, double* Bck1, double* RHS);
    /* Function that updates the blocktridiagonal matrix, once it was
     * partially decomposed */
    void UpdateNextBlocks(double* UpdatingB, double* Omegab, double* Thetab,
            double* RHSv, double* newOmegab, double* newThetab,
            double* newRHSv);
    void UpdateNextBlocks(double* UpdatingB, double* Block, double* RHSv,
            double* newBlock, double* newRHSv);
    /* Functions used to set up the blocks of the central system */
    void MatrixMatrixProduct(double* SolutionB, double* M_L, double* M_R);
    void AddBlockAndMatrixMatrixProduct(
            double* SolutionB, double* AddingB, double* M_L, double* M_R);
    void MatrixVectorProduct(double* SolutionV, double* M_L, double* V_R);
    void AddBlockAndMatrixVectorProduct(
            double* SolutionV, double* AddingV, double* M_L, double* V_R);
    /* Functions that use theta and omega to calculate the final
     * solution */
    void CalculateFinalSolution(double* FS, double* Omegab, double* Thetab,
            double* betav, std::size_t sv);
    void CalculateFinalSolution(
            double* FS, double* OmegaOrThetab, double* betav, std::size_t sv);
    /* Auxiliar Functions */
    static void
    CopyVector(double* VectorCopy, double* VectorCopied, std::size_t n);
    /* Functions for dealing with partitions and threads */
    std::size_t
    setUpPartition(std::size_t nPartitions_, const std::size_t nOfBlocks_);
    std::size_t
    setUpThreads(const std::size_t nThreads_, const std::size_t nPartitions_);
};

} // namespace blots

#endif /* SRC_CDMBTS_H */
