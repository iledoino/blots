#ifndef SRC_BLOTS_H
#define SRC_BLOTS_H

/*
 * Blots is a parallel linear system solver for matrices that are block
 * tridiagonal. The non-diagonal blocks have the option to be either
 * uniform or with a certain number of null rows, which the routines in
 * the software do not require to be filled.
 *
 * Blots is focused on performance and optimal memory storage. The linear
 * system solvers in the library do not require increasing the matrix sparsity,
 * and therefore they rely on block-diagonal dominance. To solve small
 * linear systems with fully stored matrices, the LU method with partial
 * pivoting is used. To solve block triadiagonal linear systems sequentially,
 * the block LU method is used. This latter has 4 alternative formulations,
 * which we apply depending on the context
 * (https://doi.org/10.1007/s10915-020-01279-w). To solve linear systems with
 * block tridiagonal matrices, the conquer and divide method is used.
 *
 * The general speedup of the conquer and divide method are 7p/22 for uniform
 * blocks and p/7 for non-uniform blocks
 * (https://doi.org/10.1007/s10915-020-01279-w), that is if we compare it to the
 * block LU method. The number p here represents the number of available
 * processors.
 */

// Linear solver for fully stored matrices
#include "lu.h"

// Linear solver for block tridiagonal matrices
#include "lumbts.h"
#include "lumnubts.h"

// Wrapper for calling proper solvers depending on uniformity of blocks
#include "nubts.h"

// Parallel linear solvers for block tridiagonal matrices
#include "cdmbts.h"
#include "cdmnubts.h"

#endif /* SRC_BLOTS_H */
