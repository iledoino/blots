#include "lumnubts.h"
#include "util.h"
#include <stdexcept>

namespace blots
{

/*
 * Constructor
 */
LUMNUBTS::LUMNUBTS(const std::size_t sqrBcksDim_,
        const std::size_t rectBcksDim_, const std::size_t nSqrBcks_,
        const double& upperCondLimit_)
    : fullLU(sqrBcksDim_)
    , upperCond(1.)
    , upperCondLimit(upperCondLimit_)
    , sqrBcksDim(sqrBcksDim_)
    , rectBcksDim(rectBcksDim_)
    , zeroBcksDim(sqrBcksDim_ - rectBcksDim_)
    , sqrBcksSize(sqrBcksDim_ * sqrBcksDim_)
    , rectBcksSize(sqrBcksDim_ * rectBcksDim_)
    , zeroBcksSize((sqrBcksDim_ - rectBcksDim_) * sqrBcksDim_)
    , sqrArraySize(nSqrBcks_ * sqrBcksDim_ * sqrBcksDim_)
    , rectArraySize((nSqrBcks_ - 1) * rectBcksDim_ * sqrBcksDim_)
    , RHSArraySize(nSqrBcks_ * sqrBcksDim_)
    , nSqrBcks(nSqrBcks_)
    , nRectBcks(nSqrBcks_ - 1)
    , ipvi(sqrBcksDim * nSqrBcks)
{
}

void
LUMNUBTS::AddMatrixMatrixProduct(
        double* SolutionB, double* M_L, double* M_R, const double ct)
{
    double tmp;

    for (std::size_t t = 0, tt = zeroBcksSize; tt != sqrBcksSize;
            t += sqrBcksDim, tt += sqrBcksDim)
        for (std::size_t i = zeroBcksDim, k = 0; i != sqrBcksDim;
                ++i, k += sqrBcksDim) {
            tmp = ct * M_L[t + i];
            for (std::size_t j = 0; j != sqrBcksDim; ++j)
                SolutionB[tt + j] += tmp * M_R[k + j];
        }
}

int
LUMNUBTS::blockDecomp(double* D, double* L, double* U)
{
    int returned_value = 1;
    double upperCondAux = 0.;
    upperCond = 1.0;

    for (std::size_t i = 0, iN = sqrBcksSize, j = 0, k = 0,
                     l = sqrArraySize - sqrBcksSize;
            i < l;
            i = iN, iN += sqrBcksSize, j += sqrBcksDim, k += rectBcksSize) {

        if (fullLU.decomp(D + i, ipvi.data() + j, 1.0e-16, upperCondAux) != 1)
            returned_value = 0;
        upperCond *= upperCondAux * 0.5;
        fullLU.forwBackSubsT(D + i, L + k, rectBcksDim, ipvi.data() + j);

        AddMatrixMatrixProduct(D + iN, L + k, U + k, -1.0);
    }
    if (fullLU.decomp(D + sqrArraySize - sqrBcksSize,
                ipvi.data() + RHSArraySize - sqrBcksDim, 1.0e-16, upperCondAux)
            != 1)
        returned_value = 0;
    upperCond *= upperCondAux;

    if (returned_value == 0) {
        std::cout << "LUMNUBTS::blockDecomp: Error: found a singular "
                     "diagonal block."
                  << std::endl;
    }
    else if (upperCond > upperCondLimit) {
        std::cout << "LUMNUBTS::blockDecomp: Error: the condition "
                     "number of the non-uniform block tridiagonal matrix is "
                     "greater than "
                  << upperCondLimit << "." << std::endl;
        returned_value = -1;
    }

    return returned_value;
}

int
LUMNUBTS::blockDecomp(
        std::vector<double>& D, std::vector<double>& L, std::vector<double>& U)
{
    return blockDecomp(D.data(), L.data(), U.data());
}

void
LUMNUBTS::blockForwSubs(double* L, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSBcksSize(sqrBcksDim * nOfRHS),
            zeroRHSBcksSize(zeroBcksDim * nOfRHS);

    for (std::size_t i = 0, iN = RHSBcksSize, j = 0;
            i < RHSBcksSize * (nSqrBcks - 1);
            i = iN, iN += RHSBcksSize, j += rectBcksSize) {
        // b_{i+1} = b_{i+1} - B_i*b_i
        block_dot(L + j, RHS + i, RHS + iN + zeroRHSBcksSize, rectBcksDim,
                sqrBcksDim, nOfRHS, -1.0);
    }
}

void
LUMNUBTS::blockForwSubsT(double* D, double* U, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSLD(sqrBcksDim * (nSqrBcks - 1));
    std::size_t RHSLDpp(RHSLD + zeroBcksDim);

    // Y_1 = Y_1 * (D_1)^{-1}
    fullLU.forwBackSubsT(D, RHS, nOfRHS, ipvi.data(), RHSLD);

    for (std::size_t ii = sqrBcksDim, i = 0, iN = sqrBcksDim, iU = 0,
                     iD = sqrBcksSize;
            ii <= RHSLD; ii += sqrBcksDim, i = iN, iN += sqrBcksDim,
                     iU += rectBcksSize, iD += sqrBcksSize) {
        // Y_{i+1} = Y_{i+1} - Y_i * B_i
        block_dot(RHS + i + zeroBcksDim, U + iU, RHS + iN, nOfRHS, rectBcksDim,
                sqrBcksDim, -1.0, RHSLDpp, 0, RHSLD);

        // Y_{i+1} = Y_{i+1} * D_{i+1}^{-1}
        fullLU.forwBackSubsT(D + iD, RHS + iN, nOfRHS, ipvi.data() + ii, RHSLD);
    }
}

void
LUMNUBTS::blockForwSubsT(std::vector<double>& D, std::vector<double>& U,
        std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockForwSubsT(D.data(), U.data(), RHS.data(), nOfRHS);
}

void
LUMNUBTS::blockForwSubs(
        std::vector<double>& L, std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockForwSubs(L.data(), RHS.data(), nOfRHS);
}

void
LUMNUBTS::blockBackSubs(double* D, double* U, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSBcksSize(sqrBcksDim * nOfRHS),
            zeroRHSBcksSize(zeroBcksDim * nOfRHS);

    // Y_M = (D_M)^{-1}*Y_M
    fullLU.forwBackSubs(D + sqrArraySize - sqrBcksSize,
            RHS + RHSBcksSize * (nSqrBcks - 1), nOfRHS,
            ipvi.data() + RHSArraySize - sqrBcksDim);

    for (std::size_t ii = (nSqrBcks - 1), i = sqrBcksDim * (nSqrBcks - 2),
                     iU = rectBcksSize * (nSqrBcks - 2),
                     iD = sqrBcksSize * (nSqrBcks - 2),
                     iRHS = RHSBcksSize * (nSqrBcks - 2),
                     iRHSN = iRHS + RHSBcksSize;
            ii > 0; ii--, i -= sqrBcksDim, iU -= rectBcksSize,
                     iD -= sqrBcksSize, iRHSN = iRHS, iRHS -= RHSBcksSize) {
        // Y_{i-1} = Y_{i-1} - S_i*Y_i
        block_dot(U + iU, RHS + iRHSN, RHS + iRHS + zeroRHSBcksSize,
                rectBcksDim, sqrBcksDim, nOfRHS, -1.0);

        // Y_{i-1} = (D_{i-1})^{-1}*Y_{i-1}
        fullLU.forwBackSubs(D + iD, RHS + iRHS, nOfRHS, ipvi.data() + i);
    }
}

void
LUMNUBTS::blockBackSubs(std::vector<double>& D, std::vector<double>& U,
        std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockBackSubs(D.data(), U.data(), RHS.data(), nOfRHS);
}

void
LUMNUBTS::blockBackSubsT(double* L, double* RHS, std::size_t nOfRHS)
{
    std::size_t RHSLD(sqrBcksDim * (nSqrBcks - 1));
    std::size_t RHSLDpp(RHSLD + zeroBcksDim);

    for (std::size_t i = sqrBcksDim * (nSqrBcks - 2), iN = i + sqrBcksDim,
                     j = rectBcksSize * (nSqrBcks - 2);
            iN > 0; iN = i, i -= sqrBcksDim, j -= rectBcksSize) {
        // b_{i+1} = b_{i+1} - b_i*B_i
        block_dot(RHS + iN + zeroBcksDim, L + j, RHS + i, nOfRHS, rectBcksDim,
                sqrBcksDim, -1.0, RHSLDpp, 0, RHSLD);
    }
}

void
LUMNUBTS::blockBackSubsT(
        std::vector<double>& L, std::vector<double>& RHS, std::size_t nOfRHS)
{
    blockBackSubsT(L.data(), RHS.data(), nOfRHS);
}

int
LUMNUBTS::solve(
        double* D, double* L, double* U, double* RHS, std::size_t nOfRHS)
{
    int i = blockDecomp(D, L, U);
    blockForwSubs(L, RHS, nOfRHS);
    blockBackSubs(D, U, RHS, nOfRHS);
    return i;
}

int
LUMNUBTS::solve(std::vector<double>& D, std::vector<double>& L,
        std::vector<double>& U, std::vector<double>& RHS, std::size_t nOfRHS)
{
    return solve(D.data(), L.data(), U.data(), RHS.data(), nOfRHS);
}

int
LUMNUBTS::solveT(
        double* D, double* L, double* U, double* RHS, std::size_t nOfRHS)
{
    int i = blockDecomp(D, L, U);
    blockForwSubsT(D, U, RHS, nOfRHS);
    blockBackSubsT(L, RHS, nOfRHS);
    return i;
}

int
LUMNUBTS::solveT(std::vector<double>& D, std::vector<double>& L,
        std::vector<double>& U, std::vector<double>& RHS, std::size_t nOfRHS)
{
    return solveT(D.data(), L.data(), U.data(), RHS.data(), nOfRHS);
}

} // namespace blots