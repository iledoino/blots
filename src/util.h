#ifndef BLOTS_UTIL_H
#define BLOTS_UTIL_H

#include <functional>

namespace blots
{

template <typename T>
using FillFunc = std::function<T(std::size_t, std::size_t, std::size_t)>;

template <typename T>
void
fill_blocks(FillFunc<T> fillFunc, T* blocksPtr, std::size_t nRows,
        std::size_t nCols, std::size_t nBlocks = 1)
{
    auto blockSize = nRows * nCols;
    for (std::size_t k = 0, kk = 0; k < nBlocks; k++, kk += blockSize)
        for (std::size_t i = 0, ii = kk; i < nRows; i++, ii += nCols)
            for (std::size_t j = 0, jj = ii; j < nCols; j++, jj++)
                blocksPtr[jj] = fillFunc(i, j, k);
}

template <typename T>
void
zero_fill_blocks(T* blocksPtr, std::size_t nRows, std::size_t nCols,
        std::size_t nBlocks = 1)
{
    FillFunc<T> fillFunc = [](std::size_t, std::size_t, std::size_t) -> T {
        return static_cast<T>(0);
    };
    fill_blocks<T>(fillFunc, blocksPtr, nRows, nCols, nBlocks);
}

/*
 * Helper function to execute the operation LHS = LHS + ct * L * R, where LHS is
 * a mxp block, L is a mxn block, R is a nxp block, and ct is a constant. The
 * constants LLD, RLD and LHSLD are the leading dimensions of matrices L, R and
 * LHS respectively. These alow sub-matrix multiplications, and they should be
 * provided as the number of entries between the last entry of a row and the
 * first one of the subsequent one
 */
template <typename T>
void
block_dot(T* blocksPtrL, T* blocksPtrR, T* blocksPtrLHS, std::size_t m,
        std::size_t n, std::size_t p, const T ct = static_cast<T>(1),
        std::size_t LLD = 0, std::size_t RLD = 0, std::size_t LHSLD = 0)
{
    T tmp;
    std::size_t LDim(n + LLD), RDim(p + RLD), LHSDim(p + LHSLD);
    for (std::size_t k = 0, kn = 0, kp = 0; k < m;
            k++, kn += LDim, kp += LHSDim)
        for (std::size_t i = 0, ip = 0; i < n; i++, ip += RDim) {
            tmp = ct * blocksPtrL[kn + i];
            for (std::size_t j = 0; j < p; j++)
                blocksPtrLHS[kp + j] += tmp * blocksPtrR[ip + j];
        }
}

template <typename T>
void
triBlockM_dot(T* blocksPtrL, T* blocksPtrD, T* blocksPtrU, T* blocksPtrX,
        T* blocksPtrRHS, std::size_t rowsD, std::size_t rowsUL,
        std::size_t nBlocks, std::size_t colsRHSX)
{
    std::size_t i = 0;
    auto bSizeD = rowsD * rowsD;
    auto bSizeUL = rowsD * rowsUL;
    auto bSizeRHSX = rowsD * colsRHSX;
    auto bSizeRHSXN = (rowsD - rowsUL) * colsRHSX;
    block_dot(blocksPtrD + i * bSizeD, blocksPtrX + i * bSizeRHSX,
            blocksPtrRHS + i * bSizeRHSX, rowsD, rowsD, colsRHSX);
    block_dot(blocksPtrU + i * bSizeUL, blocksPtrX + (i + 1) * bSizeRHSX,
            blocksPtrRHS + i * bSizeRHSX + bSizeRHSXN, rowsUL, rowsD, colsRHSX);
    for (i = 1; i < (nBlocks - 1); i++) {
        block_dot(blocksPtrL + (i - 1) * bSizeUL,
                blocksPtrX + (i - 1) * bSizeRHSX,
                blocksPtrRHS + i * bSizeRHSX + bSizeRHSXN, rowsUL, rowsD,
                colsRHSX);
        block_dot(blocksPtrD + i * bSizeD, blocksPtrX + i * bSizeRHSX,
                blocksPtrRHS + i * bSizeRHSX, rowsD, rowsD, colsRHSX);
        block_dot(blocksPtrU + i * bSizeUL, blocksPtrX + (i + 1) * bSizeRHSX,
                blocksPtrRHS + i * bSizeRHSX + bSizeRHSXN, rowsUL, rowsD,
                colsRHSX);
    }
    i = nBlocks - 1;
    block_dot(blocksPtrL + (i - 1) * bSizeUL, blocksPtrX + (i - 1) * bSizeRHSX,
            blocksPtrRHS + i * bSizeRHSX + bSizeRHSXN, rowsUL, rowsD, colsRHSX);
    block_dot(blocksPtrD + i * bSizeD, blocksPtrX + i * bSizeRHSX,
            blocksPtrRHS + i * bSizeRHSX, rowsD, rowsD, colsRHSX);
}

template <typename T>
void
triBlockM_dotT(T* blocksPtrL, T* blocksPtrD, T* blocksPtrU, T* blocksPtrX,
        T* blocksPtrRHS, std::size_t rowsD, std::size_t rowsUL,
        std::size_t nBlocks, std::size_t rowsRHSX)
{
    std::size_t i = 0;
    auto bSizeD = rowsD * rowsD;
    auto bSizeUL = rowsD * rowsUL;
    auto bZeroDim = (rowsD - rowsUL);
    auto RHSXLD = rowsD * (nBlocks - 1);
    auto RHSXLDpp = rowsD * (nBlocks - 1) + bZeroDim;

    block_dot(blocksPtrX + i * rowsD, blocksPtrD + i * bSizeD,
            blocksPtrRHS + i * rowsD, rowsRHSX, rowsD, rowsD, static_cast<T>(1),
            RHSXLD, 0, RHSXLD);
    block_dot(blocksPtrX + (i + 1) * rowsD + bZeroDim, blocksPtrL + i * bSizeUL,
            blocksPtrRHS + i * rowsD, rowsRHSX, rowsUL, rowsD,
            static_cast<T>(1), RHSXLDpp, 0, RHSXLD);
    for (i = 1; i < (nBlocks - 1); i++) {
        block_dot(blocksPtrX + (i - 1) * rowsD + bZeroDim,
                blocksPtrU + (i - 1) * bSizeUL, blocksPtrRHS + i * rowsD,
                rowsRHSX, rowsUL, rowsD, static_cast<T>(1), RHSXLDpp, 0,
                RHSXLD);
        block_dot(blocksPtrX + i * rowsD, blocksPtrD + i * bSizeD,
                blocksPtrRHS + i * rowsD, rowsRHSX, rowsD, rowsD,
                static_cast<T>(1), RHSXLD, 0, RHSXLD);
        block_dot(blocksPtrX + (i + 1) * rowsD + bZeroDim,
                blocksPtrL + i * bSizeUL, blocksPtrRHS + i * rowsD, rowsRHSX,
                rowsUL, rowsD, static_cast<T>(1), RHSXLDpp, 0, RHSXLD);
    }
    i = nBlocks - 1;
    block_dot(blocksPtrX + (i - 1) * rowsD + bZeroDim,
            blocksPtrU + (i - 1) * bSizeUL, blocksPtrRHS + i * rowsD, rowsRHSX,
            rowsUL, rowsD, static_cast<T>(1), RHSXLDpp, 0, RHSXLD);
    block_dot(blocksPtrX + i * rowsD, blocksPtrD + i * bSizeD,
            blocksPtrRHS + i * rowsD, rowsRHSX, rowsD, rowsD, static_cast<T>(1),
            RHSXLD, 0, RHSXLD);
}

} // namespace blots

#endif /* BLOTS_UTIL_H */
