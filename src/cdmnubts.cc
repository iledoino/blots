#include "cdmnubts.h"
#include <vector>

namespace blots
{

std::size_t
CDMNUBTS::setUpPartition(std::size_t nPartitions_, const std::size_t nSqrBcks_)
{
    if (nSqrBcks_ >= (2 * nPartitions_ - 1))
        return nPartitions_;

    while ((nSqrBcks_ < (2 * nPartitions_ - 1)) && nPartitions_ > 0)
        --nPartitions_;

    return nPartitions_;
}

std::size_t
CDMNUBTS::setUpThreads(
        const std::size_t nThreads_, const std::size_t nPartitions_)
{
    std::size_t nThreads_p = nThreads_;

    if (nThreads_ > nPartitions_)
        nThreads_p = nPartitions_;

    threadStart = new std::size_t[nThreads_p];
    threadEnd = new std::size_t[nThreads_p];

    std::size_t rest = nPartitions_ % nThreads_p;
    std::size_t idealNumberOfIndexes = (nPartitions_ - rest) / nThreads_p;

    threadStart[0] = 0;
    for (std::size_t i = 1; i != (rest + 1); ++i)
        threadStart[i] = threadStart[i - 1] + idealNumberOfIndexes + 1;
    for (std::size_t i = (rest + 1); i != nThreads_p; ++i)
        threadStart[i] = threadStart[i - 1] + idealNumberOfIndexes;

    threadEnd[nThreads_p - 1] = (nPartitions_ - 1);
    for (std::size_t i = (nThreads_p - 1); i > 0; --i)
        threadEnd[i - 1] = threadStart[i] - 1;

    return nThreads_p;
}

void
CDMNUBTS::Allocate()
{
    D_p = new double[sqrBcksSize * (nPartitions - 1)];
    U_p = new double[rectBcksSize * (nPartitions - 1)];
    L_p = new double[rectBcksSize * (nPartitions - 1)];
    RHS_p = new double[sqrBcksDim * (nPartitions - 1)];

    Ai_D = new double*[nPartitions];
    Ai_L = new double*[nPartitions];
    Ai_U = new double*[nPartitions];
    RHSi = new double*[nPartitions];

    sMtxUnif = new std::size_t[nPartitions];
    sMtxNUnif = new std::size_t[nPartitions];
    sVector = new std::size_t[nPartitions];
    iMtxUnif = new std::size_t[nPartitions];
    iMtxNUnif = new std::size_t[nPartitions];
    iVector = new std::size_t[nPartitions];

    Theta = new double*[nPartitions - 1];
    Omega = new double*[nPartitions - 1];

    /*
     * auxiliar variables to set up sMtxUnif and sVector
     */
    std::size_t availableBlocksToDistributeAmongPartitions
            = nSqrBcks - (nPartitions - 1);
    std::size_t rest = availableBlocksToDistributeAmongPartitions % nPartitions;
    std::size_t idealNOfBlocksOnEachPartition
            = (availableBlocksToDistributeAmongPartitions - rest) / nPartitions;

    /*
     * lines of code that set up the size of the arrays for each partition
     */
    for (std::size_t i = 0; i < nPartitions; ++i) {
        sMtxUnif[i] = sqrBcksSize * idealNOfBlocksOnEachPartition;
        sMtxNUnif[i] = rectBcksSize * idealNOfBlocksOnEachPartition;
        sVector[i] = sqrBcksDim * idealNOfBlocksOnEachPartition;
    }
    for (std::size_t i = 0; i < rest; ++i) {
        sMtxUnif[i] += sqrBcksSize;
        sMtxNUnif[i] += rectBcksSize;
        sVector[i] += sqrBcksDim;
    }

    /*
     * once the sizes are defined, one can now define the indices
     */
    iMtxUnif[0] = 0;
    iMtxNUnif[0] = 0;
    iVector[0] = 0;
    for (std::size_t i = 1; i < nPartitions; ++i) {
        iMtxUnif[i] = iMtxUnif[i - 1] + sMtxUnif[i - 1] + sqrBcksSize;
        iMtxNUnif[i] = iMtxNUnif[i - 1] + sMtxNUnif[i - 1] + rectBcksSize;
        iVector[i] = iVector[i - 1] + sVector[i - 1] + sqrBcksDim;
    }

    /*
     * These array of pointers are used to more easily execute the method
     */
    for (std::size_t i = 0; i < nPartitions; ++i) {
        Ai_D[i] = D + iMtxUnif[i];
        Ai_L[i] = L + iMtxNUnif[i];
        Ai_U[i] = U + iMtxNUnif[i];
        RHSi[i] = RHS + iVector[i];
    }

    /*
     * These arrays are the extra memory used on the method
     */
    for (std::size_t i = 0; i < nPartitions - 1; ++i) {
        Theta[i] = new double[sMtxUnif[i]];
        Omega[i] = new double[sMtxUnif[i + 1]];
    }
}

CDMNUBTS::CDMNUBTS(double* D_, double* L_, double* U_, double* RHS_,
        const std::size_t sqrBcksDim_, const std::size_t rectBcksDim_,
        const std::size_t nSqrBcks_, const std::size_t nPartitions_,
        const std::size_t nThreads_)
    : D(D_)
    , L(L_)
    , U(U_)
    , RHS(RHS_)
    , sqrBcksDim(sqrBcksDim_)
    , rectBcksDim(rectBcksDim_)
    , zeroBcksDim(sqrBcksDim_ - rectBcksDim_)
    , sqrBcksSize(sqrBcksDim_ * sqrBcksDim_)
    , rectBcksSize(sqrBcksDim_ * rectBcksDim_)
    , zeroBcksSize((sqrBcksDim_ - rectBcksDim_) * sqrBcksDim_)
    , sqrArraySize(nSqrBcks_ * sqrBcksDim_ * sqrBcksDim_)
    , rectArraySize((nSqrBcks_ - 1) * rectBcksDim_ * sqrBcksDim_)
    , RHSArraySize(nSqrBcks_ * sqrBcksDim_)
    , nSqrBcks(nSqrBcks_)
    , nRectBcks(nSqrBcks_ - 1)
    , threadStart(nullptr)
    , threadEnd(nullptr)
    , nPartitions(setUpPartition(nPartitions_, nSqrBcks_))
    , nThreads(setUpThreads(nThreads_, setUpPartition(nPartitions_, nSqrBcks_)))
    , allocatedInside(false)
    , sMtxUnif(nullptr)
    , sMtxNUnif(nullptr)
    , sVector(nullptr)
    , iMtxUnif(nullptr)
    , iMtxNUnif(nullptr)
    , iVector(nullptr)
    , D_p(nullptr)
    , L_p(nullptr)
    , U_p(nullptr)
    , RHS_p(nullptr)
    , Theta(nullptr)
    , Omega(nullptr)
    , Ai_D(nullptr)
    , Ai_L(nullptr)
    , Ai_U(nullptr)
    , RHSi(nullptr)
    , upperCondLimit_(1.0e5)
    , smallSystem(sqrBcksDim_, rectBcksDim_, nPartitions_ - 1, upperCondLimit_)
{
    Allocate();
}

CDMNUBTS::CDMNUBTS(const std::size_t sqrBcksDim_,
        const std::size_t rectBcksDim_, const std::size_t nSqrBcks_,
        const std::size_t nPartitions_, const std::size_t nThreads_)
    : D(nullptr)
    , L(nullptr)
    , U(nullptr)
    , RHS(nullptr)
    , sqrBcksDim(sqrBcksDim_)
    , rectBcksDim(rectBcksDim_)
    , zeroBcksDim(sqrBcksDim_ - rectBcksDim_)
    , sqrBcksSize(sqrBcksDim_ * sqrBcksDim_)
    , rectBcksSize(sqrBcksDim_ * rectBcksDim_)
    , zeroBcksSize((sqrBcksDim_ - rectBcksDim_) * sqrBcksDim_)
    , sqrArraySize(nSqrBcks_ * sqrBcksDim_ * sqrBcksDim_)
    , rectArraySize((nSqrBcks_ - 1) * rectBcksDim_ * sqrBcksDim_)
    , RHSArraySize(nSqrBcks_ * sqrBcksDim_)
    , nSqrBcks(nSqrBcks_)
    , nRectBcks(nSqrBcks_ - 1)
    , threadStart(nullptr)
    , threadEnd(nullptr)
    , nPartitions(setUpPartition(nPartitions_, nSqrBcks_))
    , nThreads(setUpThreads(nThreads_, setUpPartition(nPartitions_, nSqrBcks_)))
    , allocatedInside(true)
    , sMtxUnif(nullptr)
    , sMtxNUnif(nullptr)
    , sVector(nullptr)
    , iMtxUnif(nullptr)
    , iMtxNUnif(nullptr)
    , iVector(nullptr)
    , D_p(nullptr)
    , L_p(nullptr)
    , U_p(nullptr)
    , RHS_p(nullptr)
    , Theta(nullptr)
    , Omega(nullptr)
    , Ai_D(nullptr)
    , Ai_L(nullptr)
    , Ai_U(nullptr)
    , RHSi(nullptr)
    , upperCondLimit_(1.0e5)
    , smallSystem(sqrBcksDim_, rectBcksDim_, nPartitions_ - 1, upperCondLimit_)
{
    D = new double[sqrBcksSize * nSqrBcks];
    L = new double[rectBcksSize * (nSqrBcks - 1)];
    U = new double[rectBcksSize * (nSqrBcks - 1)];
    RHS = new double[sqrBcksDim * nSqrBcks];

    Allocate();
}

CDMNUBTS::~CDMNUBTS()
{
    if (allocatedInside) {
        delete[] D;
        delete[] U;
        delete[] L;
        delete[] RHS;
    }
    delete[] D_p;
    delete[] U_p;
    delete[] L_p;
    delete[] RHS_p;
    delete[] sMtxUnif;
    delete[] sMtxNUnif;
    delete[] sVector;
    delete[] iMtxUnif;
    delete[] iMtxNUnif;
    delete[] iVector;
    delete[] Ai_D;
    delete[] Ai_L;
    delete[] Ai_U;
    delete[] RHSi;

    for (std::size_t i = 0; i < nPartitions - 1; ++i) {
        delete[] Theta[i];
        delete[] Omega[i];
    }

    delete[] Theta;
    delete[] Omega;

    delete[] threadEnd;
    delete[] threadStart;
}

void
CDMNUBTS::SetThetaAndOmega()
{
#ifdef _OPENMP
#pragma omp parallel default(shared) num_threads(nThreads)
    {
        std::size_t i = omp_get_thread_num();
#else
    for (std::size_t i = 0; i < nThreads; ++i) {
#endif /* _OPENMP */
        std::size_t start = threadStart[i];
        std::size_t end = (i + 1) != nThreads ? threadEnd[i] : threadEnd[i] - 1;

        for (i = start; i <= end; ++i) {
            for (std::size_t j = 0; j < (sMtxUnif[i] - rectBcksSize); j++)
                Theta[i][j] = 0.0;
            for (std::size_t j = (sMtxUnif[i] - rectBcksSize),
                             jj = (sMtxNUnif[i] - rectBcksSize);
                    j != sMtxUnif[i]; ++j, ++jj)
                Theta[i][j] = Ai_U[i][jj];

            for (std::size_t j = sMtxUnif[i + 1] - 1; j >= sqrBcksSize; j--)
                Omega[i][j] = 0.0;
            for (std::size_t j = 0; j < zeroBcksSize; ++j)
                Omega[i][j] = 0.;
            for (std::size_t j = zeroBcksSize, k = -rectBcksSize;
                    j != sqrBcksSize; ++j, ++k)
                Omega[i][j] = Ai_L[i + 1][k];
        }
    }
}

void
CDMNUBTS::MatrixMatrixProduct(double* SolutionB, double* M_L, double* M_R)
{
    double tmp;

    for (std::size_t t = 0; t != zeroBcksSize; ++t)
        SolutionB[t] = 0.;

    for (std::size_t t = 0; t != rectBcksSize; t += sqrBcksDim) {
        tmp = -M_L[t];
        for (std::size_t j = 0, k = t + zeroBcksSize; j != sqrBcksDim; ++j, ++k)
            SolutionB[k] = tmp * M_R[j];

        for (std::size_t i = 1; i != sqrBcksDim; ++i) {
            tmp = -M_L[t + i];
            for (std::size_t j = 0, k = t + zeroBcksSize, l = i * sqrBcksDim;
                    j != sqrBcksDim; ++j, ++k, ++l)
                SolutionB[k] += tmp * M_R[l];
        }
    }
}

void
CDMNUBTS::MatrixMatrixProductv2(double* SolutionB, double* M_L, double* M_R)
{
    double tmp;

    for (std::size_t t = 0; t != rectBcksSize; t += sqrBcksDim) {
        tmp = -M_L[t];
        for (std::size_t j = 0, k = t; j != sqrBcksDim; ++j, ++k)
            SolutionB[k] = tmp * M_R[j];

        for (std::size_t i = 1; i != sqrBcksDim; ++i) {
            tmp = -M_L[t + i];
            for (std::size_t j = 0, k = t, l = i * sqrBcksDim; j != sqrBcksDim;
                    ++j, ++k, ++l)
                SolutionB[k] += tmp * M_R[l];
        }
    }
}

void
CDMNUBTS::AddBlockAndMatrixMatrixProduct(
        double* SolutionB, double* AddingB, double* M_L, double* M_R)
{
    double tmp;

    for (std::size_t t = 0; t != zeroBcksSize; ++t)
        SolutionB[t] += AddingB[t];

    for (std::size_t t = 0; t != rectBcksSize; t += sqrBcksDim) {
        tmp = -M_L[t];
        for (std::size_t j = 0, k = t + zeroBcksSize; j != sqrBcksDim; ++j, ++k)
            SolutionB[k] += AddingB[k] + tmp * M_R[j];

        for (std::size_t i = 1; i != sqrBcksDim; ++i) {
            tmp = -M_L[t + i];
            for (std::size_t j = 0, k = t + zeroBcksSize, l = i * sqrBcksDim;
                    j != sqrBcksDim; ++j, ++k, ++l)
                SolutionB[k] += tmp * M_R[l];
        }
    }
}

void
CDMNUBTS::MatrixVectorProduct(double* SolutionV, double* M_L, double* V_R)
{
    for (std::size_t i = 0; i < zeroBcksDim; ++i)
        SolutionV[i] = 0.;

    for (std::size_t i = 0, k = 0, l = zeroBcksDim; i < rectBcksDim;
            i++, k += sqrBcksDim, ++l) {
        SolutionV[l] = -M_L[k] * V_R[0];
        for (std::size_t j = 1; j < sqrBcksDim; j++)
            SolutionV[l] -= M_L[k + j] * V_R[j];
    }
}

void
CDMNUBTS::AddBlockAndMatrixVectorProduct(
        double* SolutionV, double* AddingV, double* M_L, double* V_R)
{
    for (std::size_t i = 0; i < zeroBcksDim; ++i)
        SolutionV[i] += AddingV[i];

    for (std::size_t i = 0, k = 0, l = zeroBcksDim; i < rectBcksDim;
            i++, k += sqrBcksDim, ++l) {
        SolutionV[l] += AddingV[l] - M_L[k] * V_R[0];
        for (std::size_t j = 1; j < sqrBcksDim; j++)
            SolutionV[l] -= M_L[k + j] * V_R[j];
    }
}

void
CDMNUBTS::CalculateFinalSolution(double* FS, double* Omegab, double* Thetab,
        double* betav, std::size_t sv)
{
    for (std::size_t i = 0; i != sv; ++i)
        for (std::size_t jC = 0, jN = sqrBcksDim, k = i * sqrBcksDim;
                jC != sqrBcksDim; ++jC, ++jN, ++k)
            FS[i] -= Omegab[k] * betav[jC] + Thetab[k] * betav[jN];
}

void
CDMNUBTS::CalculateFinalSolution(
        double* FS, double* OmegaOrThetab, double* betav, std::size_t sv)
{
    for (std::size_t i = 0; i != sv; ++i)
        for (std::size_t j = 0, k = i * sqrBcksDim; j != sqrBcksDim; ++j, ++k)
            FS[i] -= OmegaOrThetab[k] * betav[j];
}

void
CDMNUBTS::CopyVector(double* VectorCopy, double* VectorCopied, std::size_t n)
{
    std::size_t i;

    for (i = 0; i < n; i++)
        VectorCopy[i] = VectorCopied[i];
}

int
CDMNUBTS::solve()
{
    SetThetaAndOmega();

#ifdef _OPENMP
#pragma omp parallel default(shared) num_threads(nThreads)
    {
        const std::size_t tid = omp_get_thread_num();
#else
    for (std::size_t tid = 0; tid < nThreads; ++tid) {
#endif /* _OPENMP */
        std::size_t start = threadStart[tid];
        std::size_t end = threadEnd[tid];
        std::size_t jv, jb, jbnu;

        for (std::size_t i = start; i <= end; ++i) {
            LUMNUBTS lu(sqrBcksDim, rectBcksDim, sVector[i] / sqrBcksDim,
                    upperCondLimit_);
            if (i == 0) {
                /** If the current partition is the first, then there is no
                 * Omega vector **/
                if (lu.blockDecomp(Ai_D[i], Ai_L[i], Ai_U[i]) != 1) {
                    std::cerr << "There was an error in the decomposition of "
                              << i << "-th matrix\n";
                }
                lu.blockForwSubs(Ai_L[i], RHSi[i], 1);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], RHSi[i], 1);
                // lu.blockForwSubs(Ai_L[i], Theta[i], sqrBcksDim);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], Theta[i], sqrBcksDim);
            }
            else if (i == (nPartitions - 1)) {
                /** If the current partition is the last, then there is no Theta
                 * vector **/
                if (lu.blockDecomp(Ai_D[i], Ai_L[i], Ai_U[i]) != 1) {
                    std::cerr << "There was an error in the decomposition of "
                              << i << "-th matrix\n";
                }
                lu.blockForwSubs(Ai_L[i], RHSi[i], 1);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], RHSi[i], 1);
                lu.blockForwSubs(Ai_L[i], Omega[i - 1], sqrBcksDim);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], Omega[i - 1], sqrBcksDim);

                /*
                 * =============================================================
                 *                 BLOCKS FOR CENTRAL SYSTEM
                 * =============================================================
                 */
                jv = sqrBcksDim * (i - 1);
                jb = sqrBcksSize * (i - 1);

                MatrixMatrixProduct(
                        D_p + jb, Ai_U[i] - rectBcksSize, Omega[i - 1]);
                MatrixVectorProduct(
                        RHS_p + jv, Ai_U[i] - rectBcksSize, RHSi[i]);
            }
            else {
                /** If the partition is not the first or last, then both are
                 * present **/
                if (lu.blockDecomp(Ai_D[i], Ai_L[i], Ai_U[i]) != 1) {
                    std::cerr << "There was an error in the decomposition of "
                              << i << "-th matrix\n";
                }
                lu.blockForwSubs(Ai_L[i], RHSi[i], 1);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], RHSi[i], 1);
                // lu.blockForwSubs(Ai_L[i], Theta[i], sqrBcksDim);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], Theta[i], sqrBcksDim);
                lu.blockForwSubs(Ai_L[i], Omega[i - 1], sqrBcksDim);
                lu.blockBackSubs(Ai_D[i], Ai_U[i], Omega[i - 1], sqrBcksDim);

                /*
                 * =============================================================
                 *                 BLOCKS FOR CENTRAL SYSTEM
                 * =============================================================
                 */
                jv = sqrBcksDim * (i - 1);
                jb = sqrBcksSize * (i - 1);
                jbnu = rectBcksSize * (i - 1);

                MatrixMatrixProductv2(
                        U_p + jbnu, Ai_U[i] - rectBcksSize, Theta[i]);
                MatrixMatrixProductv2(L_p + jbnu,
                        Ai_L[i] + sMtxNUnif[i] - rectBcksSize,
                        Omega[i - 1] + sMtxUnif[i] - sqrBcksSize);
                MatrixMatrixProduct(
                        D_p + jb, Ai_U[i] - rectBcksSize, Omega[i - 1]);
                MatrixVectorProduct(
                        RHS_p + jv, Ai_U[i] - rectBcksSize, RHSi[i]);
            }
        }

#ifdef _OPENMP
#pragma omp barrier

#pragma omp single
#endif /* _OPENMP */
        {
            for (std::size_t i = 0; i != (nPartitions - 1); ++i) {
                jv = sqrBcksDim * i;
                jb = sqrBcksSize * i;

                AddBlockAndMatrixMatrixProduct(D_p + jb,
                        Ai_D[i + 1] - sqrBcksSize,
                        Ai_L[i + 1] - 2 * rectBcksSize,
                        Theta[i] + sMtxUnif[i] - sqrBcksSize);
                AddBlockAndMatrixVectorProduct(RHS_p + jv,
                        RHSi[i + 1] - sqrBcksDim,
                        Ai_L[i + 1] - 2 * rectBcksSize,
                        RHSi[i] + sVector[i] - sqrBcksDim);
            }

            smallSystem.solve(D_p, L_p, U_p, RHS_p);
        }

        start = threadStart[tid];
        end = threadEnd[tid];

        for (std::size_t i = start; i <= end; ++i) {
            if (i == 0) {
                CalculateFinalSolution(RHSi[0], Theta[0], RHS_p, sVector[0]);
                CopyVector(RHSi[1] - sqrBcksDim, RHS_p, sqrBcksDim);
            }
            else if (i == (nPartitions - 1))
                CalculateFinalSolution(RHSi[nPartitions - 1],
                        Omega[nPartitions - 2],
                        RHS_p + sqrBcksDim * (nPartitions - 2),
                        sVector[nPartitions - 1]);
            else {
                CalculateFinalSolution(RHSi[i], Omega[i - 1], Theta[i],
                        RHS_p + sqrBcksDim * (i - 1), sVector[i]);
                CopyVector(RHSi[i + 1] - sqrBcksDim, RHS_p + sqrBcksDim * i,
                        sqrBcksDim);
            }
        }
    }

    return 1;
}

} // namespace blots