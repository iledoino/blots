#ifndef SRC_CDMNUBTS_H
#define SRC_CDMNUBTS_H

/*
 * This software was created by Ismael de Souza Ledoino
 * <ismael.sledoino@gmail.com>, with supervision of Duilio Tadeu
 * <dtadeu@gmail.com> (Universidade Federal Rural do Rio de Janeiro
 * - UFRRJ) and Dan Marchesin <marchesi@impa.br> (Instituto de
 * Matematica Pura e Aplicada - IMPA). Commentaries and suggestions,
 * or even questions, please email the developer.
 */
#include "lumbts.h"
#include "lumnubts.h"
#include <cmath>
#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#endif /* _OPENMP */

namespace blots
{

/*
 * Conquer and Divide Method for solving non-uniform block tridiagonal
 * linear systems in parallel (shared memory machines)
 */
class CDMNUBTS
{
public:
    CDMNUBTS(const std::size_t sqrBcksDim_, const std::size_t rectBcksDim_,
            const std::size_t nSqrBcks_, const std::size_t nPartitions_,
            const std::size_t nThreads_);
    CDMNUBTS(double* D_, double* L_, double* U_, double* RHS_,
            const std::size_t sqrBcksDim_, const std::size_t rectBcksDim_,
            const std::size_t nSqrBcks_, const std::size_t nPartitions_,
            const std::size_t nThreads_);
    virtual ~CDMNUBTS();

    /* Pointers to hold the three arrays of blocks and the right hand side */
    double *D, *L, *U, *RHS;
    /* variables to specify the number of blocks and the block's dimension */
    const std::size_t sqrBcksDim, rectBcksDim, zeroBcksDim;
    const std::size_t sqrBcksSize, rectBcksSize, zeroBcksSize;
    const std::size_t sqrArraySize, rectArraySize, RHSArraySize;
    const std::size_t nSqrBcks, nRectBcks;
    std::size_t *threadStart, *threadEnd;
    const std::size_t nPartitions, nThreads;

    /* Function that solves the linear system, returning 1 in case
     * it was succesfully solved */
    int solve();

    double upperCondLimit() const { return upperCondLimit_; }
    double& upperCondLimit() { return upperCondLimit_; }

private:
    CDMNUBTS(const CDMNUBTS&) = delete;
    CDMNUBTS& operator=(const CDMNUBTS&) = delete;

    /* This constant specifies whether the arrays D, L, and U are
     * allocated inside the constructor or not */
    const bool allocatedInside;
    /* Arrays used to deal with the several partitions */
    std::size_t *sMtxUnif, *sMtxNUnif, *sVector, *iMtxUnif, *iMtxNUnif,
            *iVector;
    /* Pointers used to solve the central Linear System*/
    double *D_p, *L_p, *U_p, *RHS_p;
    /* Pointers used to organize the Conquer and Divide Method */
    double **Theta, **Omega;
    double **Ai_D, **Ai_L, **Ai_U, **RHSi;

    /* Function called inside the constructors */
    void Allocate();
    /* Function that sets up the extra memory used on this method */
    void SetThetaAndOmega();
    /* Functions used to set up the blocks of the central system */
    void MatrixMatrixProduct(double* SolutionB, double* M_L, double* M_R);
    void MatrixMatrixProductv2(double* SolutionB, double* M_L, double* M_R);
    void AddBlockAndMatrixMatrixProduct(
            double* SolutionB, double* AddingB, double* M_L, double* M_R);
    void MatrixVectorProduct(double* SolutionV, double* M_L, double* V_R);
    void AddBlockAndMatrixVectorProduct(
            double* SolutionV, double* AddingV, double* M_L, double* V_R);
    /* Functions that use theta and omega to calculate the final
     * solution */
    void CalculateFinalSolution(double* FS, double* Omegab, double* Thetab,
            double* betav, std::size_t sv);
    void CalculateFinalSolution(
            double* FS, double* OmegaOrThetab, double* betav, std::size_t sv);

    /* Auxiliar Functions */
    static void
    CopyVector(double* VectorCopy, double* VectorCopied, std::size_t n);

    /* Class that solves the block tridiagonal system originated from
     * conquer and division decomposition */
    double upperCondLimit_;
    LUMNUBTS smallSystem;

    /* Functions for dealing with partitions and threads */
    std::size_t
    setUpPartition(std::size_t nPartitions_, const std::size_t nOfBlocks_);
    std::size_t
    setUpThreads(const std::size_t nThreads_, const std::size_t nPartitions_);
};

} // namespace blots

#endif /* SRC_CDMNUBTS_H */
