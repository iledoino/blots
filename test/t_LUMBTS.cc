#include "lumbts.h"
#include "lumnubts.h"
#include "util.h"
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace blots;

int
test(std::size_t m, std::size_t n, std::size_t r, std::size_t nOfBlocks,
        double tol, bool transpose = false)
{
    double condition = 10000.0;
    std::vector<double> L(m * n * (nOfBlocks)), D(m * m * nOfBlocks),
            U(m * n * (nOfBlocks)), X(m * nOfBlocks * r),
            RHS(m * nOfBlocks * r, 0.);
    LUMBTS lumbts(nOfBlocks, m, condition);
    LUMNUBTS lumnubts(m, n, nOfBlocks, condition);

    // defining diagonal square blocks
    FillFunc<double> fillFuncD = [=](std::size_t row, std::size_t col,
                                         std::size_t /* block */) {
        return row == col ? 10. :
                            std::cos(static_cast<double>(row * m + col) * M_PI
                                    / static_cast<double>(m * m));
    };
    fill_blocks(fillFuncD, D.data(), m, m, nOfBlocks);

    // defining upper and lower blocks
    FillFunc<double> fillFuncUL = [=](std::size_t row, std::size_t col,
                                          std::size_t /* block */) {
        return std::sin(static_cast<double>((row + 1) * (m + 2) + col + 1)
                * M_PI / static_cast<double>((m + 2) * (n + 2)));
    };
    fill_blocks(fillFuncUL, U.data(), n, m, nOfBlocks - 1);
    fill_blocks(fillFuncUL, L.data(), n, m, nOfBlocks - 1);

    // defining the real solution
    FillFunc<double> fillFuncX = [=](std::size_t row, std::size_t /* col */,
                                         std::size_t /* block */) {
        return std::sin(static_cast<double>(row) * M_PI
                / static_cast<double>(m * nOfBlocks));
    };
    fill_blocks(fillFuncX, X.data(), m, r, nOfBlocks);

// #define PRINT_MATRIX
#ifdef PRINT_MATRIX
    std::cerr << std::scientific;
    for (std::size_t i = 0; i < nOfBlocks; i++) {
        std::cerr << "D[block " << i << "]: [";
        for (std::size_t j = 0; j < m * m; j++)
            std::cerr << D[i * m * m + j] << " ";
        std::cerr << "]\n";
    }
    std::cerr << "\n";
    for (std::size_t i = 0; i < nOfBlocks - 1; i++) {
        std::cerr << "L[block " << i << "]: [";
        for (std::size_t j = 0; j < m * n; j++)
            std::cerr << L[i * m * n + j] << " ";
        std::cerr << "]\n";
    }
    std::cerr << "\n";
    for (std::size_t i = 0; i < nOfBlocks - 1; i++) {
        std::cerr << "U[block " << i << "]: [";
        for (std::size_t j = 0; j < m * n; j++)
            std::cerr << U[i * m * n + j] << " ";
        std::cerr << "]\n";
    }
    std::cerr << "\n";
#endif

    // Calculation of RHS
    if (transpose) {
        triBlockM_dotT(L.data(), D.data(), U.data(), X.data(), RHS.data(), m, n,
                nOfBlocks, r);
    }
    else {
        triBlockM_dot(L.data(), D.data(), U.data(), X.data(), RHS.data(), m, n,
                nOfBlocks, r);
    }

    // solving the linear system
    if (m == n) {
        if (lumbts.blockDecomp(D.data(), L.data(), U.data()) == 1) {
            if (transpose) {
                lumbts.blockForwSubsT(D.data(), U.data(), RHS.data(), r);
                lumbts.blockBackSubsT(L.data(), RHS.data(), r);
            }
            else {
                lumbts.blockForwSubs(L.data(), RHS.data(), r);
                lumbts.blockBackSubs(D.data(), U.data(), RHS.data(), r);
            }
        }
        else
            std::cerr << "There was an error in the decomposition\n";
    }
    else {
        if (lumnubts.blockDecomp(D.data(), L.data(), U.data()) == 1) {
            if (transpose) {
                lumnubts.blockForwSubsT(D.data(), U.data(), RHS.data(), r);
                lumnubts.blockBackSubsT(L.data(), RHS.data(), r);
            }
            else {
                lumnubts.blockForwSubs(L.data(), RHS.data(), r);
                lumnubts.blockBackSubs(D.data(), U.data(), RHS.data(), r);
            }
        }
        else
            std::cerr << "There was an error in the decomposition\n";
    }

    // calculating the error
    std::cout << std::scientific;
    double norm = 0;
    for (std::size_t i = 0; i < m * nOfBlocks * r; i++) {
        norm = std::max(std::abs(X[i] - RHS[i]), norm);
    }

// #define PRINT_RHS_AND_X
#ifdef PRINT_RHS_AND_X
    std::cerr << std::scientific;
    for (std::size_t i = 0; i < m * nOfBlocks * r; i++) {
        std::cerr << "X[" << i << "]: " << X[i] << ", RHS[" << i
                  << "]: " << RHS[i] << ", abs(diff)[" << i
                  << "]: " << std::abs(X[i] - RHS[i]) << "\n";
    }
#endif

    std::string solverName = m == n ? "LUMBTS" : "LUMNUBTS";
    std::cout << solverName << ": The error for m=" << m << ", n=" << n
              << ", r=" << r << ", and nOfBlocks=" << nOfBlocks << " is "
              << norm << std::endl;

    int failed = norm > tol;

    return failed;
}

int
main(int argc, char* argv[])
{
    assert(argc == 2 || argc == 6 || argc == 7);

    if (argc == 2) {
        int code;
        std::stringstream ss(argv[1]);
        ss >> code;
        std::cerr << "Proceeding with default test, code: " << code << "\n";

        switch (code) {
        case 0:
            std::cerr << "tridiagonal system works:\n";
            return test(1, 1, 1, 3, 1.0e-15);
            break;

        case 1:
            std::cerr << "block-tridiagonal system works:\n";
            return test(2, 2, 1, 3, 1.0e-15);
            break;

        case 2:
            std::cerr << "block-tridiagonal system with multiple RHS works:\n";
            return test(2, 2, 2, 3, 1.0e-15);
            break;

        case 3:
            std::cerr << "large block-tridiagonal system with large blocks "
                         "works:\n";
            return test(50, 50, 30, 1000, 1.0e-10);
            break;

        case 4:
            std::cerr << "block-tridiagonal system works:\n";
            return test(2, 1, 1, 3, 1.0e-15);
            break;

        case 5:
            std::cerr << "block-tridiagonal system with multiple RHS works:\n";
            return test(2, 1, 2, 3, 1.0e-15);
            break;

        case 6:
            std::cerr << "large block-tridiagonal system with large diagonal "
                         "blocks "
                         "and small non-diagonal blocks works:\n";
            return test(50, 1, 30, 1000, 1.0e-10);
            break;

        case 7:
            std::cerr << "large block-tridiagonal system with large diagonal "
                         "blocks "
                         "and large non-diagonal blocks works:\n";
            return test(50, 49, 30, 1000, 1.0e-4);
            break;

        case 8:
            std::cerr << "block-tridiagonal system works (XA=RHS):\n";
            return test(2, 2, 1, 3, 1.0e-15, true);
            break;

        case 9:
            std::cerr << "block-tridiagonal system with multiple RHS works "
                         "(XA=RHS):\n";
            return test(2, 2, 2, 3, 1.0e-15, true);
            break;

        case 10:
            std::cerr << "large block-tridiagonal system with large blocks "
                         "works (XA=RHS):\n";
            return test(50, 50, 30, 1000, 1.0e-10, true);
            break;

        case 11:
            std::cerr << "block-tridiagonal system works (XA=RHS):\n";
            return test(2, 1, 1, 3, 1.0e-15, true);
            break;

        case 12:
            std::cerr << "block-tridiagonal system with multiple RHS works "
                         "(XA=RHS):\n";
            return test(2, 1, 2, 3, 1.0e-15, true);
            break;

        case 13:
            std::cerr << "large block-tridiagonal system with large diagonal "
                         "blocks "
                         "and small non-diagonal blocks works (XA=RHS):\n";
            return test(50, 1, 30, 1000, 1.0e-10, true);
            break;

        case 14:
            std::cerr << "large block-tridiagonal system with large diagonal "
                         "blocks "
                         "and large non-diagonal blocks works (XA=RHS):\n";
            return test(50, 49, 30, 1000, 1.0e-4, true);
            break;

        default:
            return 1;
            break;
        }
    }

    std::stringstream ssm(argv[1]);
    std::size_t m;
    ssm >> m;

    std::stringstream ssn(argv[2]);
    std::size_t n;
    ssn >> n;

    std::stringstream ssr(argv[3]);
    std::size_t r;
    ssr >> r;

    std::stringstream ssnob(argv[4]);
    std::size_t nob;
    ssnob >> nob;

    std::stringstream sstol(argv[5]);
    double tol;
    sstol >> tol;
    assert(tol > 0.);

    bool transpose = false;
    if (argc == 7) {
        std::stringstream sstranspose(argv[6]);
        sstranspose >> transpose;
    }

    return test(m, n, r, nob, tol, transpose);
}