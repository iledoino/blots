#include "lu.h"
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace blots;

int
test(std::size_t n, std::size_t m, double tol)
{
    std::vector<double> matrix(n * n), x(m * n), rhs(m * n);
    std::vector<std::size_t> p(n);

    for (std::size_t i = 0; i < n; ++i) {
        for (std::size_t j = 0; j < n; j++) {
            matrix[i * n + j] = std::cos(static_cast<double>(i * n + j) * M_PI
                    / static_cast<double>(n * n));
        }
        matrix[i * n + i] = 1.0;
        for (std::size_t j = 0; j < m; j++) {
            x[i * m + j] = static_cast<double>(m) + 1.0;
            rhs[i * m + j] = 0.;
        }
    }

    for (std::size_t i = 0; i < n; ++i) {
        for (std::size_t j = 0; j < n; j++) {
            for (std::size_t k = 0; k < m; k++) {
                rhs[i * m + k] += matrix[i * n + j] * x[j * m + k];
            }
        }
    }

    LU fullLU(n);

    double zero(1.0e-16), condition = 0.;
    fullLU.decomp(matrix.data(), p.data(), zero, condition);
    fullLU.forwBackSubs(matrix.data(), rhs.data(), m, p.data());

    double norm(0.);
    for (std::size_t i = 0; i < n; ++i) {
        for (std::size_t j = 0; j < m; j++) {
            norm = std::max(norm, std::abs(x[i * m + j] - rhs[i * m + j]));
        }
    }

    std::cerr << "LU: The error with n=" << n << ", m=" << m << " is --> "
              << norm << std::endl;

    int failed = norm > tol;

    return failed;
}

int
main(int argc, char* argv[])
{
    assert(argc == 2 || argc == 4);

    if (argc == 2) {
        int code;
        std::stringstream ss(argv[1]);
        ss >> code;
        std::cerr << "Proceeding with default test, code: " << code << "\n";

        switch (code) {
        case 0:
            std::cerr << "1D system works:\n";
            return test(1, 1, 1.0e-16);
            break;

        case 1:
            std::cerr << "2D system works:\n";
            return test(2, 1, 1.0e-15);
            break;

        case 2:
            std::cerr << "2D system with multiple RHS works:\n";
            return test(2, 2, 1.0e-15);
            break;

        case 3:
            std::cerr << "nD system with n large works:\n";
            return test(1000, 1, 1.0e-4);
            break;

        case 4:
            std::cerr << "nD system with n large and many RHS works:\n";
            return test(1000, 100, 1.0e-4);
            break;

        default:
            return 1;
            break;
        }
    }

    std::stringstream ssn(argv[1]);
    std::size_t n;
    ssn >> n;

    std::stringstream ssm(argv[2]);
    std::size_t m;
    ssm >> m;

    std::stringstream sstol(argv[3]);
    double tol;
    sstol >> tol;
    assert(tol > 0.);

    return test(n, m, tol);
}
