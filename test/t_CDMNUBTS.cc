#include "cdmnubts.h"
#include "util.h"
#include <iostream>
#include <omp.h>
#include <vector>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace blots;

int
main(void)
{
    std::size_t m = 40, n = 3;
    std::size_t p = m - n;
    std::size_t nOfBlocks = 1000;
#ifdef _OPENMP
    std::cerr << "Open MP found" << std::endl;
    std::size_t nThreads = omp_get_max_threads();
#else
    std::cerr << "Open MP not found" << std::endl;
    std::size_t nThreads = 3;
#endif /* _OPENMP */

    std::vector<double> L(m * n * (nOfBlocks)), D(m * m * nOfBlocks),
            U(m * n * (nOfBlocks)), X(m * nOfBlocks), RHS(m * nOfBlocks, 0.);

    CDMNUBTS mySystem(D.data(), L.data(), U.data(), RHS.data(), m, n, nOfBlocks,
            nThreads, nThreads);

    // defining diagonal square blocks
    FillFunc<double> fillFuncD = [=](std::size_t row, std::size_t col,
                                         std::size_t /* block */) {
        return row == col ? 10. :
                            std::cos(static_cast<double>(row * m + col) * M_PI
                                    / static_cast<double>(m * m));
    };
    fill_blocks(fillFuncD, D.data(), m, m, nOfBlocks);

    // defining upper and lower blocks
    FillFunc<double> fillFuncUL = [=](std::size_t row, std::size_t col,
                                          std::size_t /* block */) {
        return std::sin(static_cast<double>((row + 1) * (m + 2) + col + 1)
                * M_PI / static_cast<double>((m + 2) * (n + 2)));
    };
    fill_blocks(fillFuncUL, U.data(), n, m, nOfBlocks - 1);
    fill_blocks(fillFuncUL, L.data(), n, m, nOfBlocks - 1);

    // defining the real solution
    FillFunc<double> fillFuncX = [=](std::size_t row, std::size_t /* col */,
                                         std::size_t /* block */) {
        return std::sin(static_cast<double>(row) * M_PI
                / static_cast<double>(m * nOfBlocks));
    };
    fill_blocks(fillFuncX, X.data(), m, 1, nOfBlocks);

    // Calculation of RHS
    triBlockM_dot(L.data(), D.data(), U.data(), X.data(), RHS.data(), m, n,
            nOfBlocks, 1);

    // solving the linear system
    if (!mySystem.solve())
        std::cerr << "There was an error in the decomposition\n";

    // calculating the error
    std::cout << std::scientific;
    double norm = 0;
    for (std::size_t i = 0; i < m * nOfBlocks; i++) {
        norm = std::max(std::abs(X[i] - RHS[i]), norm);
    }

    double tol = 1.0e-10;
    std::cout << "CDMNUBTS: The error for m=" << m << ", n=" << n
              << ", and nOfBlocks=" << nOfBlocks << " is " << norm << std::endl;

    int failed = norm > tol;

    return failed;

    return 0;
}
