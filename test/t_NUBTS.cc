#include "nubts.h"
#include "util.h"
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace blots;

int
test(const std::size_t m, const std::size_t n, const std::size_t nOfBlocks,
        const double condition_inverse, const double tol)
{
    std::size_t p = m - n;
    std::vector<double> X(m * nOfBlocks);
    NUBTS mySystem(m, n, nOfBlocks, condition_inverse);

    // defines a diagonal difference, so that the system is diagonally-dominant
    std::vector<double> diagDiff(m);
    FillFunc<double> fillFuncDiagDiff = [=](std::size_t row, std::size_t,
                                                std::size_t) {
        return 100. * std::sin(static_cast<double>(row + 1) * M_PI / (row + 2));
    };
    fill_blocks(fillFuncDiagDiff, diagDiff.data(), m, 1, 1);

    // defining diagonal blocks
    const double c1 = static_cast<double>(m * m * nOfBlocks);
    FillFunc<double> fillFuncD = [=](std::size_t row, std::size_t col,
                                         std::size_t block) {
        return std::sin(static_cast<double>(block * m * m + row * m + col) * 2.
                       * M_PI / c1)
                + (row == col ? diagDiff[row] : 0.);
    };
    fill_blocks(fillFuncD, mySystem.mDiag, m, m, nOfBlocks);

    // defining upper rectangular blocks
    const double c2 = static_cast<double>((m + 2) * (n + 2) * (nOfBlocks + 2));
    FillFunc<double> fillFuncU = [=](std::size_t row, std::size_t col,
                                         std::size_t block) {
        return std::sin(static_cast<double>((block + 1) * (m + 2) * (n + 2)
                                + (row + 1) * (m + 2) + col + 1)
                * 2. * M_PI / c2);
    };
    fill_blocks(fillFuncU, mySystem.mUpper, n, m, nOfBlocks - 1);

    // defining lower rectangular blocks
    const double c3 = static_cast<double>(m * n * nOfBlocks);
    FillFunc<double> fillFuncL = [=](std::size_t row, std::size_t col,
                                         std::size_t block) {
        return std::cos(static_cast<double>(block * m * n + row * m + col) * 2.
                * M_PI / c3);
    };
    fill_blocks(fillFuncL, mySystem.mLower, n, m, nOfBlocks - 1);

    // defining the real solution
    FillFunc<double> fillFuncX
            = [=](std::size_t row, std::size_t /* col */, std::size_t block) {
                  return std::sin(static_cast<double>(block * m * 1 + row)
                          * M_PI / static_cast<double>(m * nOfBlocks));
              };
    fill_blocks(fillFuncX, X.data(), m, 1, nOfBlocks);

    // Calculation of RHS
    zero_fill_blocks(mySystem.RHS, m, 1, nOfBlocks);
    triBlockM_dot(mySystem.mLower, mySystem.mDiag, mySystem.mUpper, X.data(),
            mySystem.RHS, m, n, nOfBlocks, 1);

    // solving the linear system
    mySystem.solve();

    // calculating the error
    double norm = 0;
    for (std::size_t i = 0; i < m * nOfBlocks; i++) {
        if (std::abs(X[i] - mySystem.RHS[i]) > norm)
            norm = std::abs(X[i] - mySystem.RHS[i]);
    }
    std::cout << std::scientific;
    std::cout << "NUBTS: the error for m=" << m << ", n=" << n
              << ", nOfBlocks=" << nOfBlocks << " is " << norm << std::endl;

    int failed = norm > tol;

    return failed;
}

int
main(int argc, char* argv[])
{
    assert(argc == 2 || argc == 6);

    if (argc == 2) {
        int code;
        std::stringstream ss(argv[1]);
        ss >> code;
        std::cerr << "Proceeding with default test, code: " << code << "\n";

        switch (code) {
        case 0:
            std::cerr << "linear system with highly non-uniform "
                         "block-tridiagonal "
                         "matrix works:\n";
            return test(8, 1, 3000, 1.0e-8, 1.0e-10);
            break;

        case 1:
            std::cerr << "linear system with slightly non-uniform "
                         "block-tridiagonal "
                         "circular matrix works:\n";
            return test(8, 7, 3000, 1.0e-8, 1.0e-10);
            break;

        case 2:
            std::cerr << "linear system with uniform block-tridiagonal matrix "
                         "works:\n";
            return test(8, 8, 3000, 1.0e-8, 1.0e-10);
            break;

        default:
            return 1;
            break;
        }
    }

    std::stringstream ssm(argv[1]);
    std::size_t m;
    ssm >> m;

    std::stringstream ssn(argv[2]);
    std::size_t n;
    ssn >> n;

    std::stringstream ssnOfBlocks(argv[3]);
    std::size_t nOfBlocks;
    ssnOfBlocks >> nOfBlocks;

    std::stringstream sscondition_inverse(argv[4]);
    double condition_inverse;
    sscondition_inverse >> condition_inverse;
    assert(condition_inverse > 0.);

    std::stringstream sstol(argv[5]);
    double tol;
    sstol >> tol;
    assert(tol > 0.);

    return test(m, n, nOfBlocks, condition_inverse, tol);
}