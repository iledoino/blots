# BLOTS - Parallel Block Tridiagonal Linear System Solver

BLOTS is set of routines created for the purpose of effectively solving block triadiagonal linear systems. The software uses block-LU and conquer and divide algorithms to solve sequentially and in parallel, respectivelly. These algorithms are focused on keeping the sparsity of the block tridiagonal matrix as close as possible to the original one during the solving proccess, therefore saving memory and computations. 

## Introduction

The most basic class in BLOTS is the `LU` class, which solves fully stored small linear systems. Creating the `LU` class simply requires passing the size of the linear system. Using it to solve a linear system requires also storing a permutation array. Two types of linear systems are solved using this class: the classic linear system AX=B, and also XA=B, where the latter has the restriction that all matrices have same dimension. For decomposing the matrix A, one uses the `decomp` function. For solving the linear system using the decomposition, one uses either `forwBackSubs` or `forwBackSubsT` function. Below is a simple example of use of the `LU` class, in which a linear system AX=B is solved, with A being 3 by 3 and B being 3 by 2
```C++
#include <iostream>
#include <vector>
#include <blots.h>

int main()
{
	std::size_t sys_dim = 3;
	std::vector<std::size_t> perm(sys_dim);
	double upperCond;
	static const double TOL_COND = 1.0e6;
	/* allocation is row major */
	std::vector<double> matrix({2., 1., -1.,\
	                            1., -1., 2.,\
								-1., 2., 1.});
	std::vector<double> x({1., -1.,\
	                       1., -1.,\
						   1., -1.});
	std::vector<double> rhs({2., -2.,\
	                         2., -2.,\
						     2., -2.});
	/* decomposition */
	LU lu(sys_dim);
	if (not lu.decomp(matrix, perm, upperCond) ||
		upperCond > TOL_COND)
		return 1;
	/* forward and backward substitution */
	lu.forwBackSubs(matrix, rhs, perm);
    /* error */
    double error = 0.0;
    for (std::size_t i=0 ; i<rhs.size() ; ++i)
        error = std::max(error, std::abs(x[i]-rhs[i]));
    std::cout << "Error: " << error << std::endl;
}
```

Building from the `LU` class, the `LUMBTS` and `LUMNUBTS` classes solve block tridiagonal linear systems using the block LU algorithm (there are actually 4 versions of them in this case, which we refer to https://doi.org/10.1007/s10915-020-01279-w). The former is speciallized to block tridiagonal linear systems with uniform blocks. The latter allows blocks which are not at the diagonal to have some rows filled with zeros. These empty rows need not be stored, and are not filled during the solving process. Both algorithms require block diagonal dominance. The decomposition is made through function `blockDecomp`, and forward and backward substitutions are made in functions `blockForwSubs` and `blockBackSubs`. Alternativelly, function `solve` wraps up the call to all of these, solving the linear system in one step. 

- [] Define the structure of the readme
- [] Instruct the use of basic sequential classes
- [] Instruct the use of parallel classes
- [] Write about documentation
- [] Write about licensing 
